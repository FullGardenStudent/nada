

  #define _LCDML_DISP_cfg_cursor                     0x7E   // cursor Symbol
  #define _LCDML_DISP_cfg_scrollbar                  1      // enable a scrollbar

  // LCD object
  // for i2c there are many different steps for initialization, some are listed here
  // when the rows and cols are not set here, they have to be set in the setup
  //LiquidCrystal_I2C lcd(0x27);  // Set the LCD I2C address
  //LiquidCrystal_I2C lcd(0x27, BACKLIGHT_PIN, POSITIVE);  // Set the LCD I2C address
  //LiquidCrystal_I2C lcd(0x27,_LCDML_DISP_cols,_LCDML_DISP_rows);
//  LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address // Addr, En, Rw, Rs, d4, d5, d6, d7, backlighpin, polarity
  LiquidCrystal_I2C lcd(0x27, 40, 4, LCD_5x8DOTS);  // Set the LCD I2C address // Addr, Columns, Rows, Character size

  const uint8_t scroll_bar[5][8] = {
    {B10001, B10001, B10001, B10001, B10001, B10001, B10001, B10001}, // scrollbar top
    {B11111, B11111, B10001, B10001, B10001, B10001, B10001, B10001}, // scroll state 1
    {B10001, B10001, B11111, B11111, B10001, B10001, B10001, B10001}, // scroll state 2
    {B10001, B10001, B10001, B10001, B11111, B11111, B10001, B10001}, // scroll state 3
    {B10001, B10001, B10001, B10001, B10001, B10001, B11111, B11111}  // scrollbar bottom
  };
byte conditionerBitmap[8] = {B01110, B01010, B01010, B01110, B01110, B11111, B11111, B01110};
byte humidifierBitmap[8]  = {B00100, B00100, B01010, B01010, B10001, B10001, B10001, B01110};
byte alterterBitmap[8]    = {B00100, B01110, B01110, B01110, B11111, B11111, B00100, B00000};
//byte arrow_Char[8] = {B00000, B00000, B10000, B10000, B10111, B10011, B10101, B01000};
byte irrigatorBitmap[8]   = {B10000, B10000, B11111, B10001, B10000, B00001, B00000, B00001};
byte luminatorBitmap[8]   = {B00000, B11111, B00000, B11111, B00000, B11111, B00000, B11111};

boolean dyn_menu_is_displayed = false;  


// *********************************************************************
// LCDML MENU/DISP
// *********************************************************************
  // LCDML_0        => layer 0
  // LCDML_0_X      => layer 1
  // LCDML_0_X_X    => layer 2
  // LCDML_0_X_X_X  => layer 3
  // LCDML_0_...      => layer ...

  // LCDML_add(id, prev_layer, new_num, lang_char_array, callback_function)
  LCDML_add         (0 , LCDML_0        , 1  , "Conditioner"    , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (1 , LCDML_0        , 2  , "Humidifier"     , NULL);        // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (2 , LCDML_0        , 3  , "Carbonizer"     , NULL);                    // NULL = no menu function
  LCDML_add         (3 , LCDML_0        , 4  , "Ozonizer"       , NULL);                    // NULL = no menu function
  LCDML_add         (4 , LCDML_0        , 5  , "Clarifier"      , NULL);                    // NULL = no menu function
  LCDML_add         (5 , LCDML_0        , 6  , "Regulator"      , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (6 , LCDML_0        , 7  , "Purifier"       , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (7 , LCDML_0        , 8  , "Sterilizer"     , NULL);                    // NULL = no menu function
  LCDML_add         (8 , LCDML_0        , 9  , "Aerator"        , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (9 , LCDML_0        , 10 , "Reservoir"      , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (10 , LCDML_0       , 11 , "Chamber"        , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (11 , LCDML_0       , 12 , "Mixer"          , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (12 , LCDML_0       , 13 , "Balancer"       , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (13 , LCDML_0       , 14 , "Supplementer"   , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (14 , LCDML_0       , 15 , "Luminator"      , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (15 , LCDML_0       , 16 , "Harmonizer"     , NULL);       // this menu function can be found on "LCDML_display_menuFunction" tab

// Example for conditions (for example for a screensaver)
  // 1. define a condition as a function of a boolean type -> return false = not displayed, return true = displayed
  // 2. set the function name as callback (remove the braces '()' it gives bad errors)
  // LCDMenuLib_add(id, prev_layer,     new_num, condition,   lang_char_array, callback_function, parameter (0-255), menu function type  )
  LCDML_addAdvanced (16 , LCDML_0       , 17  , COND_hide,  "screensaver"        , mFunc_screensaver,        0,   _LCDML_TYPE_default);       // this menu function can be found on "LCDML_display_menuFunction" tab
  
//Conditioner SubMenu
  LCDML_addAdvanced (17 , LCDML_0_1     , 1  , NULL              , NULL      , mFunc_para_Conditioner,            0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (18 , LCDML_0_1     , 2  , NULL              , NULL      , mDyn_para_ConditionerUnits,        0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (19 , LCDML_0_1     , 3  , NULL              , NULL      , mDyn_para_ConditionerSetpoint,     0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (20 , LCDML_0_1     , 4  , NULL              , NULL      , mDyn_para_ConditionerTrigger,      0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_add         (21 , LCDML_0_1     , 5  , "Schedule"        , NULL);                    // NULL = no menu function
  LCDML_addAdvanced (22 , LCDML_0_1_5   , 1  , NULL              , NULL      , mDyn_para_ConditionerSchdOffMode,  0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (23 , LCDML_0_1_5   , 2  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (24 , LCDML_0_1_5   , 3  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_add         (25 , LCDML_0_1_5   , 4  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (26 , LCDML_0_1_5   , 5  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (27 , LCDML_0_1     , 6  , "Calibrate"       , mFunc_jumpTo_timer_info); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (28 , LCDML_0_1     , 7  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (29 , LCDML_0_1     , 8  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab

//Humidifier SubMenu
  LCDML_addAdvanced (30 , LCDML_0_2     , 1  , NULL              , NULL      , mFunc_para_Humidifier,             0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (31 , LCDML_0_2     , 2  , NULL              , NULL      , mDyn_para_HumidifierSetpoint,      0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (32 , LCDML_0_2     , 3  , NULL              , NULL      , mDyn_para_HumidifierTrigger,       0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_add         (33 , LCDML_0_2     , 4  , "Schedule"        , NULL);                    // NULL = no menu function
  LCDML_addAdvanced (34 , LCDML_0_2_4   , 1  , NULL              , NULL      , mDyn_para_HumidifierSchdOffMode,   0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (35 , LCDML_0_2_4   , 2  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (36 , LCDML_0_2_4   , 3  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_add         (37 , LCDML_0_2_4   , 4  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (38 , LCDML_0_2_4   , 5  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (39 , LCDML_0_2     , 5  , "Calibrate"       , mFunc_jumpTo_timer_info); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (40 , LCDML_0_2     , 6  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (41 , LCDML_0_2     , 7  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab

//Carbonizer SubMenu
  LCDML_addAdvanced (42 , LCDML_0_3     , 1  , NULL              , " "      , mFunc_para_Carbonizer,             0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (43 , LCDML_0_3     , 2  , NULL              , " "      , mDyn_para_CarbonizerSetpoint,      0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (44 , LCDML_0_3     , 3  , NULL              , " "      , mDyn_para_CarbonizerTrigger,       0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_add         (45 , LCDML_0_3     , 4  , "Schedule"        , NULL);                    // NULL = no menu function
  LCDML_addAdvanced (46 , LCDML_0_3_4   , 1  , NULL              , " "      , mDyn_para_CarbonizerSchdOffMode,   0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (47 , LCDML_0_3_4   , 2  , NULL              , " "      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (48 , LCDML_0_3_4   , 3  , NULL              , " "      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_add         (49 , LCDML_0_3_4   , 4  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (50 , LCDML_0_3_4   , 5  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (51 , LCDML_0_3     , 5  , "Calibrate"       , mFunc_jumpTo_timer_info); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (52 , LCDML_0_3     , 6  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (53 , LCDML_0_3     , 7  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab

//Ozonizer SubMenu
  LCDML_addAdvanced (54 , LCDML_0_4     , 1  , NULL              , NULL      , mFunc_para_Ozonizer,             0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (55 , LCDML_0_4     , 2  , NULL              , NULL      , mDyn_para_OzonizerSetpoint,      0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (56 , LCDML_0_4     , 3  , NULL              , NULL      , mDyn_para_OzonizerTrigger,       0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_add         (57 , LCDML_0_4     , 4  , "Schedule"        , NULL);                    // NULL = no menu function
  LCDML_addAdvanced (58 , LCDML_0_4_4   , 1  , NULL              , NULL      , mDyn_para_OzonizerSchdOffMode,   0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (59 , LCDML_0_4_4   , 2  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (60 , LCDML_0_4_4   , 3  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_add         (61 , LCDML_0_4_4   , 4  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (62 , LCDML_0_4_4   , 5  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (63 , LCDML_0_4     , 5  , "Calibrate"       , mFunc_jumpTo_timer_info); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (64 , LCDML_0_4     , 6  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (65 , LCDML_0_4     , 7  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab

//Clarifier SubMenu
  LCDML_addAdvanced (66 , LCDML_0_5     , 1  , NULL              , NULL      , mFunc_para_Clarifier,             0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (67 , LCDML_0_5     , 2  , NULL              , NULL      , mDyn_para_ClarifierSetpoint,      0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (68 , LCDML_0_5     , 3  , NULL              , NULL      , mDyn_para_ClarifierTrigger,       0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_add         (69 , LCDML_0_5     , 4  , "Schedule"        , NULL);                    // NULL = no menu function
  LCDML_addAdvanced (70 , LCDML_0_5_4   , 1  , NULL              , NULL      , mDyn_para_ClarifierSchdOffMode,   0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (71 , LCDML_0_5_4   , 2  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (72 , LCDML_0_5_4   , 3  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_add         (73 , LCDML_0_5_4   , 4  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (74 , LCDML_0_5_4   , 5  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (75 , LCDML_0_5     , 5  , "Calibrate"       , mFunc_jumpTo_timer_info); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (76 , LCDML_0_5     , 6  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (77 , LCDML_0_5     , 7  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab

//Regulator SubMenu
  LCDML_addAdvanced (78 , LCDML_0_6     , 1  , NULL              , NULL      , mFunc_para_Regulator,             0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (79 , LCDML_0_6     , 2  , NULL              , NULL      , mDyn_para_RegulatorSetpoint,      0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (80 , LCDML_0_6     , 3  , NULL              , NULL      , mDyn_para_RegulatorTrigger,       0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_add         (81 , LCDML_0_6     , 4  , "Schedule"        , NULL);                    // NULL = no menu function
  LCDML_addAdvanced (82 , LCDML_0_6_4   , 1  , NULL              , NULL      , mDyn_para_RegulatorSchdOffMode,   0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (83 , LCDML_0_6_4   , 2  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (84 , LCDML_0_6_4   , 3  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_add         (85 , LCDML_0_6_4   , 4  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (86 , LCDML_0_6_4   , 5  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (87 , LCDML_0_6     , 5  , "Calibrate"       , mFunc_jumpTo_timer_info); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (88 , LCDML_0_6     , 6  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (89 , LCDML_0_6     , 7  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab

//Purifier SubMenu
  LCDML_addAdvanced (90 , LCDML_0_7     , 1  , NULL              , NULL      , mFunc_para_Purifier,             0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (91 , LCDML_0_7     , 2  , NULL              , NULL      , mDyn_para_PurifierSetpoint,      0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_addAdvanced (92 , LCDML_0_7     , 3  , NULL              , NULL      , mDyn_para_PurifierTrigger,       0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
  LCDML_add         (93 , LCDML_0_7     , 4  , "Schedule"        , NULL);                    // NULL = no menu function
  LCDML_addAdvanced (94 , LCDML_0_7_4   , 1  , NULL              , NULL      , mDyn_para_PurifierSchdOffMode,   0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (95 , LCDML_0_7_4   , 2  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_addAdvanced (96 , LCDML_0_7_4   , 3  , NULL              , NULL      , mDyn_time,                         0,   _LCDML_TYPE_dynParam);                     // NULL = no menu function
  LCDML_add         (97 , LCDML_0_7_4   , 4  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (98 , LCDML_0_7_4   , 5  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (99 , LCDML_0_7     , 5  , "Calibrate"       , mFunc_jumpTo_timer_info); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (100, LCDML_0_7     , 6  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab
  LCDML_add         (101, LCDML_0_7     , 7  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab


//Reservoir SubMenu
//  LCDML_addAdvanced (38 , LCDML_0_5     , 1  , NULL              , "Setpoint"      , mDyn_para_ReservoirSetpoint,       0,   _LCDML_TYPE_dynParam);                    // NULL = no menu function
//  LCDML_add         (42 , LCDML_0_5     , 3  , "Home"            , mFunc_goToRootMenu);      // this menu function can be found on "LCDML_display_menuFunction" tab
//  LCDML_add         (43 , LCDML_0_5     , 4  , "Back"            , mFunc_back); // this menu function can be found on "LCDML_display_menuFunction" tab

  // ***TIP*** Try to update _LCDML_DISP_cnt when you add a menu element.

  // menu element count - last element id
  // this value must be the same as the last menu element
  #define _LCDML_DISP_cnt    101

  // create menu
  LCDML_createMenu(_LCDML_DISP_cnt);

// *********************************************************************
// SETUP
// *********************************************************************
  void LCD_setup()
  {
    // serial init; only be needed if serial control is used
//    Serial.begin(9600);                // start serial
//    Serial.println(F(_LCDML_VERSION)); // only for examples

    // LCD Begin
    //lcd.init();
//    lcd.begin(_LCDML_DISP_cols,_LCDML_DISP_rows);
    lcd.begin();

    lcd.backlight();
    //lcd.begin(_LCDML_DISP_cols,_LCDML_DISP_rows);  // some display types needs here the initialization

    
    // set special chars for scrollbar
    lcd.createChar(0, (uint8_t*)scroll_bar[0]);
    lcd.createChar(1, (uint8_t*)scroll_bar[1]);
    lcd.createChar(2, (uint8_t*)scroll_bar[2]);
    lcd.createChar(3, (uint8_t*)scroll_bar[3]);
    lcd.createChar(4, (uint8_t*)scroll_bar[4]);
    
    lcd.createChar(11, conditionerBitmap);
    lcd.createChar(12, humidifierBitmap);
    lcd.createChar(13, irrigatorBitmap);
    lcd.createChar(14, luminatorBitmap);

    
    // LCDMenuLib Setup
    LCDML_setup(_LCDML_DISP_cnt);

    // Some settings which can be used

    // Enable Menu Rollover
    LCDML.MENU_enRollover();

    // Enable Screensaver (screensaver menu function, time to activate in ms)
    LCDML.SCREEN_enable(mFunc_screensaver, 10000); // set to 30 seconds
    //LCDML.SCREEN_disable();

    // Some needful methods

    // You can jump to a menu function from anywhere with
    //LCDML.OTHER_jumpToFunc(mFunc_p2); // the parameter is the function name
  }


//*********************************************************************************
//LCDML functions
//*********************************************************************************
 // =====================================================================
//
// CONTROL
//
// =====================================================================
// *********************************************************************
// *********************************************************************
// content:
// (0) Control over serial interface  with asdw_e_q
// (1) Control over one analog input
// (2) Control over 4 - 6 digital input pins (internal pullups enabled)
// (3) Control over encoder [third party lib] (Download: https://github.com/PaulStoffregen/Encoder)
// (4) Control with Keypad  [third party lib] (Download: http://playground.arduino.cc/Main/KeypadTutorial )
// (5) Control with an IR remote [third party lib] (Download: https://github.com/z3t0/Arduino-IRremote )
// (6) Control with a joystick
// (7) Control over I2C PCF8574
// *********************************************************************

#define _LCDML_CONTROL_cfg      3

// theory:
// "#if" is a preprocessor directive and no error, look here:
// (English) https://en.wikipedia.org/wiki/C_preprocessor
// (German)  https://de.wikipedia.org/wiki/C-Pr%C3%A4prozessor


// *********************************************************************
// *************** (0) CONTROL OVER SERIAL INTERFACE *******************
// *********************************************************************
#if(_LCDML_CONTROL_cfg == 0)
// settings
  # define _LCDML_CONTROL_serial_enter           'e'
  # define _LCDML_CONTROL_serial_up              'w'
  # define _LCDML_CONTROL_serial_down            's'
  # define _LCDML_CONTROL_serial_left            'a'
  # define _LCDML_CONTROL_serial_right           'd'
  # define _LCDML_CONTROL_serial_quit            'q'
// *********************************************************************
void lcdml_menu_control(void)
{
  // If something must init, put in in the setup condition
  if(LCDML.BT_setup()) {
    // runs only once
  }

  // check if new serial input is available
  if (Serial.available()) {
    // read one char from input buffer
    switch (Serial.read())
    {
      case _LCDML_CONTROL_serial_enter:  LCDML.BT_enter(); break;
      case _LCDML_CONTROL_serial_up:     LCDML.BT_up();    break;
      case _LCDML_CONTROL_serial_down:   LCDML.BT_down();  break;
      case _LCDML_CONTROL_serial_left:   LCDML.BT_left();  break;
      case _LCDML_CONTROL_serial_right:  LCDML.BT_right(); break;
      case _LCDML_CONTROL_serial_quit:   LCDML.BT_quit();  break;
      default: break;
    }
  }
}

// *********************************************************************
// ******************************* END *********************************
// *********************************************************************





// *********************************************************************
// *************** (1) CONTROL OVER ONE ANALOG PIN *********************
// *********************************************************************
#elif(_LCDML_CONTROL_cfg == 1)

  unsigned long g_LCDML_DISP_press_time = 0;
// settings
  #define _LCDML_CONTROL_analog_pin              0
  // when you did not use a button set the value to zero
  #define _LCDML_CONTROL_analog_enter_min        850     // Button Enter
  #define _LCDML_CONTROL_analog_enter_max        920
  #define _LCDML_CONTROL_analog_up_min           520     // Button Up
  #define _LCDML_CONTROL_analog_up_max           590
  #define _LCDML_CONTROL_analog_down_min         700     // Button Down
  #define _LCDML_CONTROL_analog_down_max         770
  #define _LCDML_CONTROL_analog_back_min         950     // Button Back
  #define _LCDML_CONTROL_analog_back_max         1020
  #define _LCDML_CONTROL_analog_left_min         430     // Button Left
  #define _LCDML_CONTROL_analog_left_max         500
  #define _LCDML_CONTROL_analog_right_min        610     // Button Right
  #define _LCDML_CONTROL_analog_right_max        680
// *********************************************************************

void lcdml_menu_control(void)
{
  // If something must init, put in in the setup condition
  if(LCDML.BT_setup()) {
    // runs only once
  }
  // check debounce timer
  if((millis() - g_LCDML_DISP_press_time) >= 200) {
    g_LCDML_DISP_press_time = millis(); // reset debounce timer

    uint16_t value = analogRead(_LCDML_CONTROL_analog_pin);  // analog pin for keypad

    if (value >= _LCDML_CONTROL_analog_enter_min && value <= _LCDML_CONTROL_analog_enter_max) { LCDML.BT_enter(); }
    if (value >= _LCDML_CONTROL_analog_up_min    && value <= _LCDML_CONTROL_analog_up_max)    { LCDML.BT_up();    }
    if (value >= _LCDML_CONTROL_analog_down_min  && value <= _LCDML_CONTROL_analog_down_max)  { LCDML.BT_down();  }
    if (value >= _LCDML_CONTROL_analog_left_min  && value <= _LCDML_CONTROL_analog_left_max)  { LCDML.BT_left();  }
    if (value >= _LCDML_CONTROL_analog_right_min && value <= _LCDML_CONTROL_analog_right_max) { LCDML.BT_right(); }
    if (value >= _LCDML_CONTROL_analog_back_min  && value <= _LCDML_CONTROL_analog_back_max)  { LCDML.BT_quit();  }
  }
}
// *********************************************************************
// ******************************* END *********************************
// *********************************************************************






// *********************************************************************
// *************** (2) CONTROL OVER DIGITAL PINS ***********************
// *********************************************************************
#elif(_LCDML_CONTROL_cfg == 2)
// settings
  unsigned long g_LCDML_DISP_press_time = 0;

  #define _LCDML_CONTROL_digital_low_active      0    // 0 = high active (pulldown) button, 1 = low active (pullup)
                                                      // http://playground.arduino.cc/CommonTopics/PullUpDownResistor
  #define _LCDML_CONTROL_digital_enable_quit     1
  #define _LCDML_CONTROL_digital_enable_lr       1
  #define _LCDML_CONTROL_digital_enter           8
  #define _LCDML_CONTROL_digital_up              9
  #define _LCDML_CONTROL_digital_down            10
  #define _LCDML_CONTROL_digital_quit            11
  #define _LCDML_CONTROL_digital_left            12
  #define _LCDML_CONTROL_digital_right           13
// *********************************************************************
void lcdml_menu_control(void)
{
  // If something must init, put in in the setup condition
  if(LCDML.BT_setup()) {
    // runs only once
    // init buttons
    pinMode(_LCDML_CONTROL_digital_enter      , INPUT_PULLUP);
    pinMode(_LCDML_CONTROL_digital_up         , INPUT_PULLUP);
    pinMode(_LCDML_CONTROL_digital_down       , INPUT_PULLUP);
    # if(_LCDML_CONTROL_digital_enable_quit == 1)
      pinMode(_LCDML_CONTROL_digital_quit     , INPUT_PULLUP);
    # endif
    # if(_LCDML_CONTROL_digital_enable_lr == 1)
      pinMode(_LCDML_CONTROL_digital_left     , INPUT_PULLUP);
      pinMode(_LCDML_CONTROL_digital_right    , INPUT_PULLUP);
    # endif
  }

  #if(_LCDML_CONTROL_digital_low_active == 1)
  #  define _LCDML_CONTROL_digital_a !
  #else
  #  define _LCDML_CONTROL_digital_a
  #endif

  uint8_t but_stat = 0x00;

  bitWrite(but_stat, 0, _LCDML_CONTROL_digital_a(digitalRead(_LCDML_CONTROL_digital_enter)));
  bitWrite(but_stat, 1, _LCDML_CONTROL_digital_a(digitalRead(_LCDML_CONTROL_digital_up)));
  bitWrite(but_stat, 2, _LCDML_CONTROL_digital_a(digitalRead(_LCDML_CONTROL_digital_down)));
  #if(_LCDML_CONTROL_digital_enable_quit == 1)
  bitWrite(but_stat, 3, _LCDML_CONTROL_digital_a(digitalRead(_LCDML_CONTROL_digital_quit)));
  #endif
  #if(_LCDML_CONTROL_digital_enable_lr == 1)
  bitWrite(but_stat, 4, _LCDML_CONTROL_digital_a(digitalRead(_LCDML_CONTROL_digital_left)));
  bitWrite(but_stat, 5, _LCDML_CONTROL_digital_a(digitalRead(_LCDML_CONTROL_digital_right)));
  #endif

  if (but_stat > 0) {
    if((millis() - g_LCDML_DISP_press_time) >= 200) {
      g_LCDML_DISP_press_time = millis(); // reset press time

      if (bitRead(but_stat, 0)) { LCDML.BT_enter(); }
      if (bitRead(but_stat, 1)) { LCDML.BT_up();    }
      if (bitRead(but_stat, 2)) { LCDML.BT_down();  }
      if (bitRead(but_stat, 3)) { LCDML.BT_quit();  }
      if (bitRead(but_stat, 4)) { LCDML.BT_left();  }
      if (bitRead(but_stat, 5)) { LCDML.BT_right(); }
    }
  }
}
// *********************************************************************
// ******************************* END *********************************
// *********************************************************************






// *********************************************************************
// *************** (3) CONTROL WITH ENCODER ****************************
// *********************************************************************
#elif(_LCDML_CONTROL_cfg == 3)
  /*
   * Thanks to "MuchMore" (Arduino forum) to add this encoder functionality
   *
   * rotate left = Up
   * rotate right = Down
   * push = Enter
   * push long = Quit
   * push + rotate left = Left
   * push + rotate right = Right
   */

  /* encoder connection
   * button * (do not use an external resistor, the internal pullup resistor is used)
   * .-------o  Arduino Pin
   * |
   * |
   * o  /
   *   /
   *  /
   * o
   * |
   * '-------o   GND
   *
   * encoder * (do not use an external resistor, the internal pullup resistors are used)
   *
   * .---------------o  Arduino Pin A
   * |        .------o  Arduino Pin B
   * |        |
   * o  /     o  /
   *   /        /
   *  /        /
   * o        o
   * |        |
   * '--------o----o   GND (common pin)
   */

  // global defines
  #define encoder_A_pin       34    // physical pin has to be 2 or 3 to use interrupts (on mega e.g. 20 or 21), use internal pullups
  #define encoder_B_pin       36    // physical pin has to be 2 or 3 to use interrupts (on mega e.g. 20 or 21), use internal pullups
  #define encoder_button_pin  35    // physical pin , use internal pullup

  #define g_LCDML_CONTROL_button_long_press    800   // ms
  #define g_LCDML_CONTROL_button_short_press   120   // ms

  //#define ENCODER_OPTIMIZE_INTERRUPTS //Only when using pin2/3 (or 20/21 on mega)
  #include <Encoder.h> //for Encoder    Download:  https://github.com/PaulStoffregen/Encoder

  Encoder ENCODER(encoder_A_pin, encoder_B_pin);

  unsigned long  g_LCDML_CONTROL_button_press_time = 0;
  bool  g_LCDML_CONTROL_button_prev       = HIGH;

// *********************************************************************
void lcdml_menu_control(void)
// *********************************************************************
{
  // If something must init, put in in the setup condition
  if(LCDML.BT_setup())
  {
    // runs only once

    // init pins
    pinMode(encoder_A_pin      , INPUT_PULLUP);
    pinMode(encoder_B_pin      , INPUT_PULLUP);
    pinMode(encoder_button_pin  , INPUT_PULLUP);
  }

  //Volatile Variable
  long g_LCDML_CONTROL_Encoder_position = ENCODER.read();
  bool button                           = digitalRead(encoder_button_pin);

  if(g_LCDML_CONTROL_Encoder_position <= -3) {

    if(!button)
    {
      LCDML.BT_left();
      g_LCDML_CONTROL_button_prev = LOW;
      g_LCDML_CONTROL_button_press_time = -1;
    }
    else
    {
      LCDML.BT_down();
    }
    ENCODER.write(g_LCDML_CONTROL_Encoder_position+4);
  }
  else if(g_LCDML_CONTROL_Encoder_position >= 3)
  {

    if(!button)
    {
      LCDML.BT_right();
      g_LCDML_CONTROL_button_prev = LOW;
      g_LCDML_CONTROL_button_press_time = -1;
    }
    else
    {
      LCDML.BT_up();
    }
    ENCODER.write(g_LCDML_CONTROL_Encoder_position-4);
  }
  else
  {
    if(!button && g_LCDML_CONTROL_button_prev)  //falling edge, button pressed
    {
      g_LCDML_CONTROL_button_prev = LOW;
      g_LCDML_CONTROL_button_press_time = millis();
    }
    else if(button && !g_LCDML_CONTROL_button_prev) //rising edge, button not active
    {
       g_LCDML_CONTROL_button_prev = HIGH;

       if(g_LCDML_CONTROL_button_press_time < 0)
       {
         g_LCDML_CONTROL_button_press_time = millis();
         //Reset for left right action
       }
       else if((millis() - g_LCDML_CONTROL_button_press_time) >= g_LCDML_CONTROL_button_long_press)
       {
         LCDML.BT_quit();
       }
       else if((millis() - g_LCDML_CONTROL_button_press_time) >= g_LCDML_CONTROL_button_short_press)
       {
         LCDML.BT_enter();
       }
    }
  }
}
// *********************************************************************
// ******************************* END *********************************
// *********************************************************************





// *********************************************************************
// *************** (4) CONTROL WITH A KEYPAD ***************************
// *********************************************************************
#elif(_LCDML_CONTROL_cfg == 4)
// include
  // more information under http://playground.arduino.cc/Main/KeypadTutorial
  #include <Keypad.h>
// settings
  #define _LCDML_CONTROL_keypad_rows 4 // Four rows
  #define _LCDML_CONTROL_keypad_cols 3 // Three columns
// global vars
  char keys[_LCDML_CONTROL_keypad_rows][_LCDML_CONTROL_keypad_cols] = {
    {'1','2','3'},
    {'4','5','6'},
    {'7','8','9'},
    {'#','0','*'}
  };
  byte rowPins[_LCDML_CONTROL_keypad_rows] = { 9, 8, 7, 6 };  // Connect keypad COL0, COL1 and COL2 to these Arduino pins.
  byte colPins[_LCDML_CONTROL_keypad_cols] = { 12, 11, 10 };  // Create the Keypad
// objects
  Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, _LCDML_CONTROL_keypad_rows, _LCDML_CONTROL_keypad_cols );
// *********************************************************************
void lcdml_menu_control(void)
{
  // If something must init, put in in the setup condition
  if(LCDML.BT_setup()) {
    // runs only once
  }
  char key = kpd.getKey();
  if(key)  // Check for a valid key.
  {
    switch (key)
    {
      case '#': LCDML.BT_enter(); break;
      case '2': LCDML.BT_up();    break;
      case '8': LCDML.BT_down();  break;
      case '4': LCDML.BT_left();  break;
      case '6': LCDML.BT_right(); break;
      case '*': LCDML.BT_quit();  break;
      default: break;
    }
  }
}
// *********************************************************************
// ******************************* END *********************************
// *********************************************************************


// *********************************************************************
// *************** (5) CONTROL WITH IR REMOTE ***************************
// *********************************************************************
#elif(_LCDML_CONTROL_cfg == 5)
    // IR include (this lib have to be installed)
    #include <IRremote.h>
    // IR global vars
    int RECV_PIN = 11;
    // IR objects
    IRrecv irrecv(RECV_PIN);
    decode_results results;

// *********************************************************************
// change in this function the IR values to your values
void lcdml_menu_control(void)
{
  // If something must init, put in in the setup condition
  if(LCDML.BT_setup()) {
    // runs only once
    irrecv.enableIRIn(); // Start the receiver
  }

  if (irrecv.decode(&results))
  {
      // comment this line out, to check the correct code
      //Serial.println(results.value, HEX);

      // in this switch case you have to change the value 0x...1 to the correct IR code
      switch (results.value)
      {
          case 0x00000001: LCDML.BT_enter(); break;
          case 0x00000002: LCDML.BT_up();    break;
          case 0x00000003: LCDML.BT_down();  break;
          case 0x00000004: LCDML.BT_left();  break;
          case 0x00000005: LCDML.BT_right(); break;
          case 0x00000006: LCDML.BT_quit();  break;
          default: break;
      }
      irrecv.resume(); // Receive the next value
  }
}
// *********************************************************************
// ******************************* END *********************************
// *********************************************************************



// *********************************************************************
// *************** (6) CONTROL OVER JOYSTICK ***************************
// *********************************************************************
#elif(_LCDML_CONTROL_cfg == 6)
  unsigned long g_LCDML_DISP_press_time = 0;
    // settings
    #define _LCDML_CONTROL_analog_pinx A0
    #define _LCDML_CONTROL_analog_piny A1
    #define _LCDML_CONTROL_digitalread 33 //don't work with u8glib

    // when you did not use a button set the value to zero
    #define _LCDML_CONTROL_analog_up_min 612 // Button Up
    #define _LCDML_CONTROL_analog_up_max 1023
    #define _LCDML_CONTROL_analog_down_min 0 // Button Down
    #define _LCDML_CONTROL_analog_down_max 412
    #define _LCDML_CONTROL_analog_left_min 612 // Button Left
    #define _LCDML_CONTROL_analog_left_max 1023
    #define _LCDML_CONTROL_analog_right_min 0 // Button Right
    #define _LCDML_CONTROL_analog_right_max 412
// *********************************************************************
void lcdml_menu_control(void)
{
  // If something must init, put in in the setup condition
  if(LCDML.BT_setup()) {
    // runs only once
    pinMode (_LCDML_CONTROL_digitalread, INPUT);
  }
  // check debounce timer
  if((millis() - g_LCDML_DISP_press_time) >= 200) {
    g_LCDML_DISP_press_time = millis(); // reset debounce timer

    uint16_t valuex = analogRead(_LCDML_CONTROL_analog_pinx);  // analogpinx
    uint16_t valuey = analogRead(_LCDML_CONTROL_analog_piny);  // analogpinx
    uint16_t valuee = digitalRead(_LCDML_CONTROL_digitalread);  //digitalpinenter


    if (valuey >= _LCDML_CONTROL_analog_up_min && valuey <= _LCDML_CONTROL_analog_up_max) { LCDML.BT_up(); }        // up
    if (valuey >= _LCDML_CONTROL_analog_down_min && valuey <= _LCDML_CONTROL_analog_down_max) { LCDML.BT_down(); }    // down
    if (valuex >= _LCDML_CONTROL_analog_left_min && valuex <= _LCDML_CONTROL_analog_left_max) { LCDML.BT_left(); }     // left
    if (valuex >= _LCDML_CONTROL_analog_right_min && valuex <= _LCDML_CONTROL_analog_right_max) { LCDML.BT_right(); }    // right

    if(valuee == true) {LCDML.BT_enter();}    // enter

    // back buttons have to be included as menu item
    // lock at the example LCDML_back_button
  }
}
// *********************************************************************
// ******************************* END *********************************
// *********************************************************************

// *********************************************************************
// *************** (7) CONTROL OVER PCF8574 ****************************
// *********************************************************************

#elif(_LCDML_CONTROL_cfg == 7)
  unsigned long g_LCDML_DISP_press_time = 0;
  #define PCF8574_1 0x26 // I2C address for the buttons

  #define PCF8574_Pin0 254
  #define PCF8574_Pin1 253
  #define PCF8574_Pin2 251
  #define PCF8574_Pin3 247
  #define PCF8574_Pin4 239
  #define PCF8574_Pin5 223
  #define PCF8574_Pin6 191
  #define PCF8574_Pin7 127

  // Specify the PCF8574 pins here
  #define _LCDML_CONTROL_PCF8574_enable_quit    0
  #define _LCDML_CONTROL_PCF8574_enable_lr      0
  #define _LCDML_CONTROL_PCF8574_enter          PCF8574_Pin1
  #define _LCDML_CONTROL_PCF8574_up             PCF8574_Pin2
  #define _LCDML_CONTROL_PCF8574_down           PCF8574_Pin0
  #define _LCDML_CONTROL_PCF8574_left           PCF8574_Pin2
  #define _LCDML_CONTROL_PCF8574l_right         PCF8574_Pin2
  #define _LCDML_CONTROL_PCF8574_quit           PCF8574_Pin2
// **********************************************************
void lcdml_menu_control(void)
{
  // If something must init, put in in the setup condition
  if(LCDML.BT_setup()) {
    // runs only once
  }

  if((millis() - g_LCDML_DISP_press_time) >= 200) {
      g_LCDML_DISP_press_time = millis(); // reset press time

    Wire.write(0xff); // All pins as input?
    Wire.requestFrom(PCF8574_1, 1);
   if (Wire.available()) {
    switch (Wire.read())
    {
      case _LCDML_CONTROL_PCF8574_enter:  LCDML.BT_enter(); break;
      case _LCDML_CONTROL_PCF8574_up:     LCDML.BT_up();    break;
      case _LCDML_CONTROL_PCF8574_down:   LCDML.BT_down();  break;
       #if(_LCDML_CONTROL_PCF8574_enable_quit == 1)
      case _LCDML_CONTROL_PCF8574_left:   LCDML.BT_left();  break;
    #endif
       #if(_LCDML_CONTROL_PCF8574_enable_lr   == 1)
      case _LCDML_CONTROL_PCF8574l_right: LCDML.BT_right(); break;
      case _LCDML_CONTROL_PCF8574_quit:   LCDML.BT_quit();  break;
    #endif
      default: break;
    }
  }
 }
}
// *********************************************************************
// ******************************* END *********************************
// *********************************************************************


#else
  #error _LCDML_CONTROL_cfg is not defined or not in range
#endif

// =====================================================================
//
// Output function
//
// =====================================================================

/* ******************************************************************** */
void lcdml_menu_clear()
/* ******************************************************************** */
{
  lcd.clear();
  lcd.setCursor(0, 0);
}

/* ******************************************************************** */
void lcdml_menu_display()
/* ******************************************************************** */
{
  // update content
  // ***************
  if (LCDML.DISP_checkMenuUpdate()) {
    // clear menu
    // ***************
    LCDML.DISP_clear();

    // declaration of some variables
    // ***************
    // content variable
    char content_text[_LCDML_DISP_cols];  // save the content text of every menu element
    // menu element object
    LCDMenuLib2_menu *tmp;
    // some limit values
    uint8_t i = LCDML.MENU_getScroll();
    uint8_t maxi = _LCDML_DISP_rows + i;
    uint8_t n = 0;

    // check if this element has children
    if ((tmp = LCDML.MENU_getDisplayedObj()) != NULL)
    {
      // loop to display lines
      do
      {
        // check if a menu element has a condition and if the condition be true
        if (tmp->checkCondition())
        {
          // check the type off a menu element
          if(tmp->checkType_menu() == true)
          {
            // display normal content
            LCDML_getContent(content_text, tmp->getID());
            lcd.setCursor(1, n);
            lcd.print(content_text);
          }
          else
          {
            if(tmp->checkType_dynParam()) {
              tmp->callback(n);
            }
          }
          // increment some values
          i++;
          n++;
        }
      // try to go to the next sibling and check the number of displayed rows
      } while (((tmp = tmp->getSibling(1)) != NULL) && (i < maxi));
    }
  }

  if(LCDML.DISP_checkMenuCursorUpdate())
  {
    // init vars
    uint8_t n_max             = (LCDML.MENU_getChilds() >= _LCDML_DISP_rows) ? _LCDML_DISP_rows : (LCDML.MENU_getChilds());
    uint8_t scrollbar_min     = 0;
    uint8_t scrollbar_max     = LCDML.MENU_getChilds();
    uint8_t scrollbar_cur_pos = LCDML.MENU_getCursorPosAbs();
    uint8_t scroll_pos        = ((1.*n_max * _LCDML_DISP_rows) / (scrollbar_max - 1) * scrollbar_cur_pos);


    // display rows
    for (uint8_t n = 0; n < n_max; n++)
    {
      //set cursor
      lcd.setCursor(0, n);

      //set cursor char
      if (n == LCDML.MENU_getCursorPos()) {
        lcd.write(_LCDML_DISP_cfg_cursor);
      } else {
        lcd.write(' ');
      }

      // delete or reset scrollbar
      if (_LCDML_DISP_cfg_scrollbar == 1) {
        if (scrollbar_max > n_max) {
          lcd.setCursor((_LCDML_DISP_cols - 1), n);
          lcd.write((uint8_t)0);
        }
        else {
          lcd.setCursor((_LCDML_DISP_cols - 1), n);
          lcd.print(' ');
        }
      }
    }

    // display scrollbar
    if (_LCDML_DISP_cfg_scrollbar == 1) {
      if (scrollbar_max > n_max) {
        //set scroll position
        if (scrollbar_cur_pos == scrollbar_min) {
          // min pos
          lcd.setCursor((_LCDML_DISP_cols - 1), 0);
          lcd.write((uint8_t)1);
        } else if (scrollbar_cur_pos == (scrollbar_max - 1)) {
          // max pos
          lcd.setCursor((_LCDML_DISP_cols - 1), (n_max - 1));
          lcd.write((uint8_t)4);
        } else {
          // between
          lcd.setCursor((_LCDML_DISP_cols - 1), scroll_pos / n_max);
          lcd.write((uint8_t)(scroll_pos % n_max) + 1);
        }
      }
    }
  }
}

/* ===================================================================== *
 *                                                                       *
 * Menu Callback Function                                                *
 *                                                                       *
 * ===================================================================== *
 *
 * EXAMPLE CODE:

// *********************************************************************
void your_function_name(uint8_t param)
// *********************************************************************
{
  if(LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // setup
    // is called only if it is started

    // starts a trigger event for the loop function every 100 milliseconds
    LCDML.FUNC_setLoopInterval(100);
  }

  if(LCDML.FUNC_loop())           // ****** LOOP *********
  {
    // loop
    // is called when it is triggered
    // - with LCDML_DISP_triggerMenu( milliseconds )
    // - with every button status change

    // check if any button is pressed (enter, up, down, left, right)
    if(LCDML.BT_checkAny()) {
      LCDML.FUNC_goBackToMenu();
    }
  }

  if(LCDML.FUNC_close())      // ****** STABLE END *********
  {
    // loop end
    // you can here reset some global vars or delete it
  }
}


 * ===================================================================== *
 */


// *********************************************************************
void mFunc_information(uint8_t param)
// *********************************************************************
{
  if(LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // setup function
    lcd.setCursor(0, 0);
    lcd.print(F("To close this"));
    lcd.setCursor(0, 1);
    lcd.print(F("function press"));
    lcd.setCursor(0, 2);
    lcd.print(F("any button or use"));
    lcd.setCursor(0, 3);
    lcd.print(F("back button"));
  }

  if(LCDML.FUNC_loop())           // ****** LOOP *********
  {
    // loop function, can be run in a loop when LCDML_DISP_triggerMenu(xx) is set
    // the quit button works in every DISP function without any checks; it starts the loop_end function
    if(LCDML.BT_checkAny()) { // check if any button is pressed (enter, up, down, left, right)
      // LCDML_goToMenu stops a running menu function and goes to the menu
      LCDML.FUNC_goBackToMenu();
    }
  }

  if(LCDML.FUNC_close())      // ****** STABLE END *********
  {
    // you can here reset some global vars or do nothing
  }
}


// *********************************************************************
uint8_t g_func_timer_info = 0;  // time counter (global variable)
unsigned long g_timer_1 = 0;    // timer variable (global variable)
void mFunc_timer_info(uint8_t param)
// *********************************************************************
{
  if(LCDML.FUNC_setup())          // ****** SETUP *********
  {
    lcd.print(F("20 sec wait")); // print some content on first row
    g_func_timer_info = 20;       // reset and set timer
    LCDML.FUNC_setLoopInterval(100);  // starts a trigger event for the loop function every 100 milliseconds

    LCDML.TIMER_msReset(g_timer_1);
  }


  if(LCDML.FUNC_loop())           // ****** LOOP *********
  {
    // loop function, can be run in a loop when LCDML_DISP_triggerMenu(xx) is set
    // the quit button works in every DISP function without any checks; it starts the loop_end function

    // reset screensaver timer
    LCDML.SCREEN_resetTimer();

    // this function is called every 100 milliseconds

    // this method checks every 1000 milliseconds if it is called
    if(LCDML.TIMER_ms(g_timer_1, 1000)) {
      g_func_timer_info--;                // increment the value every second
      lcd.setCursor(0, 0);                // set cursor pos
      lcd.print(F("  "));
      lcd.setCursor(0, 0);                // set cursor pos
      lcd.print(g_func_timer_info);       // print the time counter value
    }

    // this function can only be ended when quit button is pressed or the time is over
    // check if the function ends normally
    if (g_func_timer_info <= 0)
    {
      // leave this function
      LCDML.FUNC_goBackToMenu();
    }
  }

  if(LCDML.FUNC_close())      // ****** STABLE END *********
  {
    // you can here reset some global vars or do nothing
  }
}


// *********************************************************************
uint8_t g_button_value = 0; // button value counter (global variable)
void mFunc_p2(uint8_t param)
// *********************************************************************
{
  if(LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // setup function
    // print LCD content
    // print LCD content
    lcd.setCursor(0, 0);
    lcd.print(F("press left or up"));
    lcd.setCursor(0, 1);
    lcd.print(F("count: 0 of 3"));
    // Reset Button Value
    g_button_value = 0;

    // Disable the screensaver for this function until it is closed
    LCDML.FUNC_disableScreensaver();

  }

  if(LCDML.FUNC_loop())           // ****** LOOP *********
  {
    // the quit button works in every DISP function without any checks; it starts the loop_end function
    if (LCDML.BT_checkAny()) // check if any button is pressed (enter, up, down, left, right)
    {
      if (LCDML.BT_checkLeft() || LCDML.BT_checkUp()) // check if button left is pressed
      {
        LCDML.BT_resetLeft(); // reset the left button
        LCDML.BT_resetUp(); // reset the left button
        g_button_value++;

        // update LCD content
        // update LCD content
        lcd.setCursor(7, 1); // set cursor
        lcd.print(g_button_value); // print change content
      }
    }

    // check if button count is three
    if (g_button_value >= 3) {
      LCDML.FUNC_goBackToMenu();      // leave this function
    }
  }

  if(LCDML.FUNC_close())     // ****** STABLE END *********
  {
    // you can here reset some global vars or do nothing
  }
}



// *********************************************************************
void mFunc_screensaver(uint8_t param)
// *********************************************************************
{
  if(LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // update LCD content
    LCDML.FUNC_setLoopInterval(2000);  // starts a trigger event for the loop function every 100 milliseconds
//    t14.enable();
    showGlobalStatus();
  }

  if(LCDML.FUNC_loop())
  {
    showGlobalStatus();
    if (LCDML.BT_checkAny()) // check if any button is pressed (enter, up, down, left, right)
    {
//      t14.disable();
      LCDML.FUNC_goBackToMenu();  // leave this function
    }
  }

  if(LCDML.FUNC_close())
  {
    // The screensaver go to the root menu
    lcdml_menu_clear();
    LCDML.MENU_goRoot();
  }
}


void showGlobalStatus()
{
  char buf[40];

//  lcd.clear();
//  lcd.setCursor(0,0);
//  int tempConditionerVal = conditionerReading;
//  sprintf(buf,"AC:%02dc HU:%02d  VE:%02dcfm CO:%02dppm CL:%02dppm",
//          tempConditionerVal,humidifierReading,ventilatorReading,carbonizerReading,clarifierReading);
//  lcd.print(buf);
//
//  lcd.setCursor(0,1);
//  sprintf(buf,"BA:%s SU:%s AE:%02dmgl CH:%02d RE:%02dc",
//          dtostrf(balancerReading, 1, 1, &buf[3]),dtostrf(supplementerReading, 1, 1, &buf[10]),chamberLevelReading,regulatorReading);  
//  lcd.print(buf);
//  
//  lcd.setCursor(0,2);
//  sprintf(buf,"RS:%02d  RY:%02dc PU:%02dntu CI:%03dph",
//          reservoirLevelReading,recyclerReading,purifierReading,circulatorReading);  
//  lcd.print(buf);
//  
//  lcd.setCursor(0,3);
//  sprintf(buf,"CY:%02dh PR:%02du FS:%03dnm LI:%02dlux",
//          lightingCycle,parReading,spectrumReading,lightIntensityReading);  
//  lcd.print(buf);
  
  lcd.clear();
  //lcd.setCursor(0,0);
  //lcd.write(1);
  lcd.setCursor(0,0);
  lcd.print("CO:");     //Conditioner
  lcd.setCursor(3,0);
  lcd.print(conditionerReading);
  //lcd.setCursor(6,0);
  //lcd.print((char) 223);
  lcd.setCursor(5,0);
  lcd.print("c");

  lcd.setCursor(7,0);
  lcd.print("HU:");     //Humidifier
  lcd.setCursor(10,0);
  lcd.print(humidifierReading);
  lcd.setCursor(12,0);
  lcd.print("%");

  lcd.setCursor(14,0);
  lcd.print("VE:");     //Ventilator
  lcd.setCursor(17,0);
  lcd.print("95");
  lcd.setCursor(19,0);
  lcd.print("cfm");

  lcd.setCursor(23,0);
  lcd.print("CA:");     //Carbonizer
  lcd.setCursor(26,0);
  lcd.print(carbonizerReading);
  lcd.setCursor(28,0);
  lcd.print("ppm");
  
  lcd.setCursor(32,0);
  lcd.print("CL:");
  lcd.setCursor(35,0);
  lcd.print("87");
  lcd.setCursor(37,0);
  lcd.print("ppm");

//  lcd.setCursor(0,2);
//  lcd.print("    Ozonizer>");
//  lcd.setCursor(13,2);
//  lcd.print("OFF");
//  lcd.setCursor(17,2);
//  lcd.print("Cur:");
//  lcd.setCursor(21,2);
//  lcd.print("75");
//  lcd.setCursor(23,2);
//  lcd.print("ppm ");
//  lcd.setCursor(28,2);
//  lcd.print("Set:");
//  lcd.setCursor(32,2);
//  lcd.print("55");
//  lcd.setCursor(34,2);
//  lcd.print("PPM");

  lcd.setCursor(0,1);
  lcd.print("BA:");
  lcd.setCursor(3,1);
  lcd.print("9.0");
  
  lcd.setCursor(7,1);
  lcd.print("SU:");
  lcd.setCursor(10,1);
  lcd.print("1.1");
//  lcd.setCursor(13,1);
//  lcd.print("u");
  
  lcd.setCursor(14,1);
  lcd.print("AE:");
  lcd.setCursor(17,1);
  lcd.print("23");
  lcd.setCursor(19,1);
  lcd.print("mgl");

  lcd.setCursor(23,1);
  lcd.print("CH:");
  lcd.setCursor(26,1);
  lcd.print("82");
  lcd.setCursor(28,1);
  lcd.print("%");

  lcd.setCursor(32,1);
  lcd.print("RE:");
  lcd.setCursor(35,1);
  lcd.print("10");
  lcd.setCursor(37,1);
  lcd.print((char) 223);
  lcd.setCursor(38,1);
  lcd.print("c");

  lcd.setCursor(0,2);
  lcd.print("RS:");
  lcd.setCursor(3,2);
  lcd.print("85");
  lcd.setCursor(5,2);
  lcd.print("%");
  
  lcd.setCursor(7,2);
  lcd.print("RY:");
  lcd.setCursor(10,2);
  lcd.print("23");
  lcd.setCursor(12,2);
  lcd.print("c");

  lcd.setCursor(14,2);
  lcd.print("PU:");
  lcd.setCursor(17,2);
  lcd.print("11");
  lcd.setCursor(19,2);
  lcd.print("ntu");

  lcd.setCursor(23,2);
  lcd.print("SR:");
  lcd.setCursor(26,2);
  lcd.print("12");
  lcd.setCursor(28,2);
  lcd.print("lph");

  lcd.setCursor(0,3);
  lcd.print("CY:");
  lcd.setCursor(3,3);
  lcd.print("12");
  lcd.setCursor(5,3);
  lcd.print("h");

  lcd.setCursor(7,3);
  lcd.print("PR:");
  lcd.setCursor(10,3);
  lcd.print("23");
  lcd.setCursor(12,3);
  lcd.print("u");

  lcd.setCursor(14,3);
  lcd.print("FS:");
  lcd.setCursor(17,3);
  lcd.print("400");
  lcd.setCursor(19,3);
  lcd.print("nm");

  lcd.setCursor(23,3);
  lcd.print("LI:");
  lcd.setCursor(26,3);
  lcd.print("23");
  lcd.setCursor(28,3);
  lcd.print("lux");
 }



// *********************************************************************
void mFunc_back(uint8_t param)
// *********************************************************************
{
  if(LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // end function and go an layer back
    LCDML.FUNC_goBackToMenu(1);      // leave this function and go a layer back
  }
}


// *********************************************************************
void mFunc_goToRootMenu(uint8_t param)
// *********************************************************************
{
  if(LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // go to root and display menu
    LCDML.MENU_goRoot();
  }
}


// *********************************************************************
void mFunc_jumpTo_timer_info(uint8_t param)
// *********************************************************************
{
  if(LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // Jump to main screen
    LCDML.OTHER_jumpToFunc(mFunc_timer_info);
  }
}

/* ===================================================================== *
 *                                                                       *
 * Conditions to show or hide a menu element on the display              *
 *                                                                       *
 * ===================================================================== *
 */



// *********************************************************************
boolean COND_hide()  // hide a menu element
// *********************************************************************
{
  return false;  // hidden
}

/* ===================================================================== *
 *                                                                       *
 * Dynamic content                                                       *
 *                                                                       *
 * ===================================================================== *
 */

// *********************************************************************
void mDyn_para_ConditionerUnits(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_ConditionerUnits == 1)
            g_dynParam_ConditionerUnits = 0;
          else if(g_dynParam_ConditionerUnits == 0)
            g_dynParam_ConditionerUnits = 1;
      }
    }
  }
  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Units   : ");
  lcd.setCursor(11, line);

  if(g_dynParam_ConditionerUnits == 0)
    lcd.print("Celsius   ");    //degrees centigrade}
  else if(g_dynParam_ConditionerUnits == 1)
    lcd.print("Fahrenheit");    //degrees Fahrenheit
}
//********************************************************************************
void mDyn_para_ConditionerTrigger(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_ConditionerTrigger == 1)
            g_dynParam_ConditionerTrigger = 0;
          else if(g_dynParam_ConditionerTrigger == 0)
            g_dynParam_ConditionerTrigger = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Trigger : ");
  lcd.setCursor(11, line);
  if(g_dynParam_ConditionerTrigger == 0)
    lcd.print("Online");
  else if(g_dynParam_ConditionerTrigger == 1)
    lcd.print("Schd  ");
}
//********************************************************************************
void mDyn_para_ConditionerSetpoint(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp())
      {
//        g_dynParam_ConditionerSetpoint--;
        g_dynParam_ConditionerSetpoint -= 0.1;
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkDown())
      {
//        g_dynParam_ConditionerSetpoint++;
        g_dynParam_ConditionerSetpoint += 0.1;
      }


      if(LCDML.BT_checkLeft())
      {
        g_dynParam_ConditionerSetpoint -= 0.1;
      }
      if(LCDML.BT_checkRight())
      {
        g_dynParam_ConditionerSetpoint += 0.1;
      }
      if (g_dynParam_ConditionerSetpoint <= CONDITIONER_SETPOINT_MIN)
        g_dynParam_ConditionerSetpoint = CONDITIONER_SETPOINT_MIN;
      else if (g_dynParam_ConditionerSetpoint >= CONDITIONER_SETPOINT_MAX)
        g_dynParam_ConditionerSetpoint = CONDITIONER_SETPOINT_MAX;        
    }
  }

//get conditionerSetpoint from EEPROM to g_dynParam_ConditionerSetpoint when entered in this menu
  char buf[40];
  sprintf (buf, "Setpoint: %s", dtostrf(g_dynParam_ConditionerSetpoint, 2, 1, &buf[10]));   //digits before decimal point: 2 , after decimal point: 1 

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print(buf);

  lcd.setCursor(16, line);
  lcd.write(223);    //degrees
  lcd.setCursor(17, line);
  if(conditionerUnits == 0)
    lcd.print("C");    //Celsius
  else if(conditionerUnits == 1)
    lcd.print("F");    //Fahrenheit
}
// *********************************************************************
void mDyn_para_ConditionerSchdOffMode(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_ConditionerSchdOffMode == 1)
            g_dynParam_ConditionerSchdOffMode = 0;
          else if(g_dynParam_ConditionerSchdOffMode == 0)
            g_dynParam_ConditionerSchdOffMode = 1;
      }
    }
  }

  char buf[40];

  // use the line from function parameters
//  lcd.setCursor(1, line);
//  lcd.print("Mode    : ");

  sprintf(buf, "Mode    : ");

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print(buf);
  
  lcd.setCursor(11, line);
  if(g_dynParam_ConditionerSchdOffMode == 0)
    lcd.print("Setpoint");
  else if(g_dynParam_ConditionerSchdOffMode == 1)
    lcd.print("Schd    ");
}
// *********************************************************************
void mDyn_para_HumidifierSetpoint(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM

      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp())
      {
        g_dynParam_HumidifierSetpoint--;
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkDown())
      {
        g_dynParam_HumidifierSetpoint++;
      }


      if(LCDML.BT_checkLeft())
      {
        g_dynParam_HumidifierSetpoint--;
      }
      if(LCDML.BT_checkRight())
      {
        g_dynParam_HumidifierSetpoint++;
      }
      if (g_dynParam_HumidifierSetpoint <= HUMIDIFIER_SETPOINT_MIN)
        g_dynParam_HumidifierSetpoint = HUMIDIFIER_SETPOINT_MIN;
      else if (g_dynParam_HumidifierSetpoint >= HUMIDIFIER_SETPOINT_MAX)
        g_dynParam_HumidifierSetpoint = HUMIDIFIER_SETPOINT_MAX;        
    }
  }

  char buf[40];
  sprintf (buf, "Setpoint: %d%", g_dynParam_HumidifierSetpoint);

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print(buf);

}
//********************************************************************************
void mDyn_para_HumidifierTrigger(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_HumidifierTrigger == 1)
            g_dynParam_HumidifierTrigger = 0;
          else if(g_dynParam_HumidifierTrigger == 0)
            g_dynParam_HumidifierTrigger = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Trigger : ");
  lcd.setCursor(11, line);
  if(g_dynParam_HumidifierTrigger == 0)
    lcd.print("Online");
  else if(g_dynParam_HumidifierTrigger == 1)
    lcd.print("Schd");
}
//********************************************************************************
void mDyn_para_HumidifierSchdOffMode(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_HumidifierSchdOffMode == 1)
            g_dynParam_HumidifierSchdOffMode = 0;
          else if(g_dynParam_HumidifierSchdOffMode == 0)
            g_dynParam_HumidifierSchdOffMode = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Mode    : ");
  lcd.setCursor(11, line);
  if(g_dynParam_HumidifierSchdOffMode == 0)
    lcd.print("Setpoint");
  else if(g_dynParam_HumidifierSchdOffMode == 1)
    lcd.print("Schd");
}
//***************************************************************************
void mDyn_para_CarbonizerSetpoint(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM

      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp())
      {
        g_dynParam_CarbonizerSetpoint--;
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkDown())
      {
        g_dynParam_CarbonizerSetpoint++;
      }


      if(LCDML.BT_checkLeft())
      {
        g_dynParam_CarbonizerSetpoint--;
      }
      if(LCDML.BT_checkRight())
      {
        g_dynParam_CarbonizerSetpoint++;
      }
      if (g_dynParam_CarbonizerSetpoint <= CARBONIZER_SETPOINT_MIN)
        g_dynParam_CarbonizerSetpoint = CARBONIZER_SETPOINT_MIN;
      else if (g_dynParam_CarbonizerSetpoint >= CARBONIZER_SETPOINT_MAX)
        g_dynParam_CarbonizerSetpoint = CARBONIZER_SETPOINT_MAX;        
    }
  }

  char buf[40];
  sprintf (buf, "Setpoint: %d%", g_dynParam_CarbonizerSetpoint);

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print(buf);

}
//********************************************************************************
void mDyn_para_CarbonizerTrigger(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_CarbonizerTrigger == 1)
            g_dynParam_CarbonizerTrigger = 0;
          else if(g_dynParam_CarbonizerTrigger == 0)
            g_dynParam_CarbonizerTrigger = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Trigger : ");
  lcd.setCursor(11, line);
  if(g_dynParam_CarbonizerTrigger == 0)
    lcd.print("Online");
  else if(g_dynParam_CarbonizerTrigger == 1)
    lcd.print("Schd");
}
//********************************************************************************
void mDyn_para_CarbonizerSchdOffMode(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_CarbonizerSchdOffMode == 1)
            g_dynParam_CarbonizerSchdOffMode = 0;
          else if(g_dynParam_CarbonizerSchdOffMode == 0)
            g_dynParam_CarbonizerSchdOffMode = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Mode    : ");
  lcd.setCursor(11, line);
  if(g_dynParam_CarbonizerSchdOffMode == 0)
    lcd.print("Setpoint");
  else if(g_dynParam_CarbonizerSchdOffMode == 1)
    lcd.print("Schd");
}
//***************************************************************************
void mDyn_para_OzonizerSetpoint(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM

      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp())
      {
        g_dynParam_OzonizerSetpoint--;
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkDown())
      {
        g_dynParam_OzonizerSetpoint++;
      }


      if(LCDML.BT_checkLeft())
      {
        g_dynParam_OzonizerSetpoint--;
      }
      if(LCDML.BT_checkRight())
      {
        g_dynParam_OzonizerSetpoint++;
      }
      if (g_dynParam_OzonizerSetpoint <= OZONIZER_SETPOINT_MIN)
        g_dynParam_OzonizerSetpoint = OZONIZER_SETPOINT_MIN;
      else if (g_dynParam_OzonizerSetpoint >= OZONIZER_SETPOINT_MAX)
        g_dynParam_OzonizerSetpoint = OZONIZER_SETPOINT_MAX;        
    }
  }

  char buf[40];
  sprintf (buf, "Setpoint: %d%", g_dynParam_OzonizerSetpoint);

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print(buf);

}
//********************************************************************************
void mDyn_para_OzonizerTrigger(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_OzonizerTrigger == 1)
            g_dynParam_OzonizerTrigger = 0;
          else if(g_dynParam_OzonizerTrigger == 0)
            g_dynParam_OzonizerTrigger = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Trigger : ");
  lcd.setCursor(11, line);
  if(g_dynParam_OzonizerTrigger == 0)
    lcd.print("Online");
  else if(g_dynParam_OzonizerTrigger == 1)
    lcd.print("Schd");
}
//********************************************************************************
void mDyn_para_OzonizerSchdOffMode(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_OzonizerSchdOffMode == 1)
            g_dynParam_OzonizerSchdOffMode = 0;
          else if(g_dynParam_OzonizerSchdOffMode == 0)
            g_dynParam_OzonizerSchdOffMode = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Mode    : ");
  lcd.setCursor(11, line);
  if(g_dynParam_OzonizerSchdOffMode == 0)
    lcd.print("Setpoint");
  else if(g_dynParam_OzonizerSchdOffMode == 1)
    lcd.print("Schd");
}
//***************************************************************************
void mDyn_para_ClarifierSetpoint(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM

      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp())
      {
        g_dynParam_ClarifierSetpoint--;
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkDown())
      {
        g_dynParam_ClarifierSetpoint++;
      }


      if(LCDML.BT_checkLeft())
      {
        g_dynParam_ClarifierSetpoint--;
      }
      if(LCDML.BT_checkRight())
      {
        g_dynParam_ClarifierSetpoint++;
      }
      if (g_dynParam_ClarifierSetpoint <= CLARIFIER_SETPOINT_MIN)
        g_dynParam_ClarifierSetpoint = CLARIFIER_SETPOINT_MIN;
      else if (g_dynParam_ClarifierSetpoint >= CLARIFIER_SETPOINT_MAX)
        g_dynParam_ClarifierSetpoint = CLARIFIER_SETPOINT_MAX;        
    }
  }

  char buf[40];
  sprintf (buf, "Setpoint: %d%", g_dynParam_ClarifierSetpoint);

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print(buf);

}
//********************************************************************************
void mDyn_para_ClarifierTrigger(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_ClarifierTrigger == 1)
            g_dynParam_ClarifierTrigger = 0;
          else if(g_dynParam_ClarifierTrigger == 0)
            g_dynParam_ClarifierTrigger = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Trigger : ");
  lcd.setCursor(11, line);
  if(g_dynParam_ClarifierTrigger == 0)
    lcd.print("Online");
  else if(g_dynParam_ClarifierTrigger == 1)
    lcd.print("Schd  ");
}
//********************************************************************************
void mDyn_para_ClarifierSchdOffMode(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_ClarifierSchdOffMode == 1)
            g_dynParam_ClarifierSchdOffMode = 0;
          else if(g_dynParam_ClarifierSchdOffMode == 0)
            g_dynParam_ClarifierSchdOffMode = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Mode    : ");
  lcd.setCursor(11, line);
  if(g_dynParam_ClarifierSchdOffMode == 0)
    lcd.print("Setpoint");
  else if(g_dynParam_ClarifierSchdOffMode == 1)
    lcd.print("Schd  ");
}
//***************************************************************************
void mDyn_para_RegulatorSetpoint(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM

      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp())
      {
        g_dynParam_RegulatorSetpoint--;
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkDown())
      {
        g_dynParam_RegulatorSetpoint++;
      }


      if(LCDML.BT_checkLeft())
      {
        g_dynParam_RegulatorSetpoint--;
      }
      if(LCDML.BT_checkRight())
      {
        g_dynParam_RegulatorSetpoint++;
      }
      if (g_dynParam_RegulatorSetpoint <= REGULATOR_SETPOINT_MIN)
        g_dynParam_RegulatorSetpoint = REGULATOR_SETPOINT_MIN;
      else if (g_dynParam_RegulatorSetpoint >= REGULATOR_SETPOINT_MAX)
        g_dynParam_RegulatorSetpoint = REGULATOR_SETPOINT_MAX;        
    }
  }

  char buf[40];
  sprintf (buf, "Setpoint: %d%", g_dynParam_RegulatorSetpoint);

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print(buf);

}
//********************************************************************************
void mDyn_para_RegulatorTrigger(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_RegulatorTrigger == 1)
            g_dynParam_RegulatorTrigger = 0;
          else if(g_dynParam_RegulatorTrigger == 0)
            g_dynParam_RegulatorTrigger = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Trigger : ");
  lcd.setCursor(11, line);
  if(g_dynParam_RegulatorTrigger == 0)
    lcd.print("Online");
  else if(g_dynParam_RegulatorTrigger == 1)
    lcd.print("Schd  ");
}
//********************************************************************************
void mDyn_para_RegulatorSchdOffMode(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_RegulatorSchdOffMode == 1)
            g_dynParam_RegulatorSchdOffMode = 0;
          else if(g_dynParam_RegulatorSchdOffMode == 0)
            g_dynParam_RegulatorSchdOffMode = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Mode    : ");
  lcd.setCursor(11, line);
  if(g_dynParam_RegulatorSchdOffMode == 0)
    lcd.print("Setpoint");
  else if(g_dynParam_RegulatorSchdOffMode == 1)
    lcd.print("Schd  ");
}
//***************************************************************************
void mDyn_para_PurifierSetpoint(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM

      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp())
      {
        g_dynParam_PurifierSetpoint--;
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkDown())
      {
        g_dynParam_PurifierSetpoint++;
      }


      if(LCDML.BT_checkLeft())
      {
        g_dynParam_PurifierSetpoint--;
      }
      if(LCDML.BT_checkRight())
      {
        g_dynParam_PurifierSetpoint++;
      }
      if (g_dynParam_PurifierSetpoint <= PURIFIER_SETPOINT_MIN)
        g_dynParam_PurifierSetpoint = PURIFIER_SETPOINT_MIN;
      else if (g_dynParam_PurifierSetpoint >= PURIFIER_SETPOINT_MAX)
        g_dynParam_PurifierSetpoint = PURIFIER_SETPOINT_MAX;        
    }
  }

  char buf[40];
  sprintf (buf, "Setpoint: %d%", g_dynParam_PurifierSetpoint);

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print(buf);

}
//********************************************************************************
void mDyn_para_PurifierTrigger(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_PurifierTrigger == 1)
            g_dynParam_PurifierTrigger = 0;
          else if(g_dynParam_PurifierTrigger == 0)
            g_dynParam_PurifierTrigger = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Trigger : ");
  lcd.setCursor(11, line);
  if(g_dynParam_PurifierTrigger == 0)
    lcd.print("Online");
  else if(g_dynParam_PurifierTrigger == 1)
    lcd.print("Schd  ");
}
//********************************************************************************
void mDyn_para_PurifierSchdOffMode(uint8_t line)
{
  // check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
        settingChanged = 1;   //use this flag to store the setting in EPROM
        
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp() || LCDML.BT_checkDown() || LCDML.BT_checkLeft() || LCDML.BT_checkRight())
      {
          if(g_dynParam_PurifierSchdOffMode == 1)
            g_dynParam_PurifierSchdOffMode = 0;
          else if(g_dynParam_PurifierSchdOffMode == 0)
            g_dynParam_PurifierSchdOffMode = 1;
      }
    }
  }

//  char buf[40];

  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("Mode    : ");
  lcd.setCursor(11, line);
  if(g_dynParam_PurifierSchdOffMode == 0)
    lcd.print("Setpoint");
  else if(g_dynParam_PurifierSchdOffMode == 1)
    lcd.print("Schd  ");
}
//***************************************************************************


// ***********************
void mDyn_time(uint8_t line)
// ***********************
{

  // reset initscreen timer when this function is displayed
  //LCDML.SCREEN_resetTimer();
  
  // check if this function is active (cursor stands on this line)
// check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
        // this function checks returns the scroll disable status (0 = menu scrolling enabled, 1 = menu scrolling disabled)
        if(LCDML.MENU_getScrollDisableStatus() == 0)
        {
          // disable the menu scroll function to catch the cursor on this point
          // now it is possible to work with BT_checkUp and BT_checkDown in this function
          // this function can only be called in a menu, not in a menu function
          LCDML.MENU_disScroll();
        }
        else
        {
          // enable the normal menu scroll function
          LCDML.MENU_enScroll();
        }

        // do something
        // ...
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkUp())
      {
        dyn_hour++;
      }

      // This check have only an effect when MENU_disScroll is set
      if(LCDML.BT_checkDown())
      {
        dyn_hour--;
      }


      if(LCDML.BT_checkLeft())
      {
        dyn_hour++;
      }
      if(LCDML.BT_checkRight())
      {
        dyn_hour--;
      }
    }
  }
  char buf[40];
  // use the line from function parameters
  lcd.setCursor(1, line);
  lcd.print("OFF Time:");
  lcd.setCursor(11, line);
  sprintf (buf, "%02d:%02d", dyn_hour, dyn_min);
  lcd.print(buf);
}

//
//uint8_t _pos = 0;
//// ***********************
//void mDyn_date(uint8_t line)
//// ***********************
//{
//
//  // check if this function is active (cursor stands on this line)
//  if (LCDML.FUNC_setup())         // ** SETUP ***
//  {
//    displayDigitalClock();
//    LCDML.FUNC_setLoopInterval(100);  // starts a trigger event for the loop function every 100 milliseconds
//  }
//
//  if (LCDML.FUNC_loop())         // ** LOOP ***
//  {
//    // This check have only an effect when MENU_disScroll is set
//    if (LCDML.BT_checkUp()) {
//      resetDigitalClock(+1);
//      //LCDML_BT_resetUp();
//      LCDML.BT_resetUp();       
//    }
//    // This check have only an effect when MENU_disScroll is set
//    if (LCDML.BT_checkDown()) {
//      resetDigitalClock(-1);
//      //LCDML_BT_resetDown();
//      LCDML.BT_resetDown();
//    }
//
//    if (LCDML.BT_checkLeft()) {
//      _pos = (_pos - 1) % 5;
//      LCDML.BT_resetLeft();
//      }
//
//    if (LCDML.BT_checkRight()) {
//      _pos = (_pos + 1) % 5;
//      //LCDML_BT_resetRight();
//      LCDML.BT_resetRight();
//    }
//  }
//  lcd.clear();
//  displayDigitalClock();
//  lcd.noBlink();
//
//  if (LCDML.FUNC_close())       // ** STABLE END ***
//  {
//    // you can here reset some global vars or do nothing
//        // disable the cursor
//    lcd.noBlink();
//  }
//}
//
//void resetDigitalClock(int8_t l_i)
//{
//  
////  DateTime now = RTC.now();
//  DateTime now = WorldClock.readTime();
//  switch (_pos)
//  {
//    case 0: RTC.adjust(DateTime(now.hour() + l_i, now.minute(), now.second(), now.day(), now.month(), now.year()));  break; // hour
//    case 1: RTC.adjust(DateTime(now.hour(), now.minute() + l_i, now.second(), now.day(), now.month(), now.year())); break; // min
//    case 2: RTC.adjust(DateTime(now.hour(), now.minute(), now.second(), now.day() + l_i, now.month(), now.year())); break; // day
//    case 3: RTC.adjust(DateTime(now.hour(), now.minute(), now.second(), now.day(), now.month() + l_i, now.year())); break; // month
//    case 4: RTC.adjust(DateTime(now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year() + l_i)); break; // year
//  }
//}
//// define help function to display the current clock settings
//void displayDigitalClock() {
//  DateTime now = nowTime;
//  lcd.setCursor(3, 0);
//  printDigits(now.hour());
//  lcd.print(":");
//  if (_pos == 1) {
//    lcd.print(" ");
//  }
//  printDigits(now.minute());
//  lcd.print(":");
//  printDigits(now.second());
//  lcd.setCursor(3, 1);
//  lcd.print(now.day());
//  lcd.print(".");
//  if (_pos == 3) {
//    lcd.print(" ");
//  }
//  lcd.print(now.month());
//  lcd.print(".");
//  if (_pos == 4) {
//    lcd.print(" ");
//  }
//  lcd.print(now.year());
//  // set cursor
//  switch (_pos) {
//    case 0: lcd.setCursor(2, 0); lcd.blink(); break; //hour
//    case 1: lcd.setCursor(6, 0); lcd.blink(); break; //min
//    case 2: lcd.setCursor(2, 1); lcd.blink(); break; //day
//    case 3: lcd.setCursor(5 + ((now.day() < 10) ? 0 : 1), 1); lcd.blink(); break; //month
//    case 4: lcd.setCursor(7 + ((now.day() < 10) ? 0 : 1) + ((now.month() < 10) ? 0 : 1), 1); lcd.blink(); break; //year
//    default: lcd.noBlink(); break;
//  }
//}
//// define help function to display digits in front of the value
//void printDigits(int digits) {
//  // utility function for digital clock display: prints preceding colon and leading 0
//  if (digits < 10) {
//    lcd.print('0');
//  }
//  lcd.print(digits);
//}

// ************************************************************************
void mFunc_para_Conditioner(uint8_t line)
{
// check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
      }
    }
  }
  char buf[40];
  // use the line from function parameters
//  lcd.setCursor(1, line);
////  lcd.print("Conditioner :");
////  lcd.setCursor(14, line);
////  sprintf (buf, "%s", dtostrf(conditionerReading, 2, 1, &buf[14]));   //digits before decimal point: 2 , after decimal point: 1
//  sprintf (buf, "%s", dtostrf(conditionerReading, 2, 1, &buf[1]));   //digits before decimal point: 2 , after decimal point: 1
//  lcd.print("Conditioner :");
//
//  lcd.print(buf);
//  lcd.setCursor(5, line);
//  
//  lcd.write(223);    //degrees centigrade
//  lcd.setCursor(6, line);
//  if(conditionerUnits == 0)
//    lcd.write("C");    //degrees centigrade
//  else if(conditionerUnits == 1)
//    lcd.print("F");    //degrees Fahrenheit
//
//  lcd.setCursor(7, line);
//  if(conditionerState == 1)
//    lcd.print(" ON ");
//  else if(conditionerState == 0)
//    lcd.print(" OFF");
//
//  lcd.setCursor(13, line);
//  if(conditionerTrigger == 0)
//    lcd.print("Online");
//  else if(conditionerTrigger == 1)
//    lcd.print("Schd");


  lcd.print(" Current :");
  lcd.setCursor(11, line);
  sprintf (buf, "%s", dtostrf(conditionerReading, 2, 1, &buf[11]));   //digits before decimal point: 2 , after decimal point: 1
  lcd.print(buf);
  
  lcd.setCursor(15, line);
  lcd.write(223);    //degrees
  lcd.setCursor(16, line);
  if(conditionerUnits == 0)
    lcd.print("C");    //Celsius
  else if(conditionerUnits == 1)
    lcd.print("F");    //Fahrenheit

  lcd.setCursor(20, line);
  if(conditionerState == 1)
    lcd.print(" ON ");
  else if(conditionerState == 0)
    lcd.print(" OFF");

  lcd.setCursor(27, line);
  if(conditionerTrigger == 0)
    lcd.print("Online");
  else if(conditionerTrigger == 1)
    lcd.print("Schd");

}

// ************************************************************************
void mFunc_para_Humidifier(uint8_t line)
{
// check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
      }
    }
  }
  char buf[40];
  // use the line from function parameters
  lcd.setCursor(1, line);
//  lcd.print("Humidifier :");
//  lcd.setCursor(14, line);
//  sprintf (buf, "%s", dtostrf(HumidifierReading, 2, 1, &buf[14]));   //digits before decimal point: 2 , after decimal point: 1
  sprintf (buf, "%d%", humidifierReading);   //digits before decimal point: 2 , after decimal point: 1
  lcd.print(buf);

  lcd.setCursor(7, line);
  if(humidifierState == 1)
    lcd.print(" ON ");
  else if(humidifierState == 0)
    lcd.print(" OFF");

  lcd.setCursor(13, line);
  if(humidifierTrigger == 0)
    lcd.print("Online");
  else if(humidifierTrigger == 1)
    lcd.print("Schd");
}

// ************************************************************************
void mFunc_para_Carbonizer(uint8_t line)
{
// check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
      }
    }
  }
  char buf[40];
  // use the line from function parameters
  lcd.setCursor(1, line);
//  lcd.print("Carbonizer :");
//  lcd.setCursor(14, line);
//  sprintf (buf, "%s", dtostrf(CarbonizerReading, 2, 1, &buf[14]));   //digits before decimal point: 2 , after decimal point: 1
  sprintf (buf, "%d%", carbonizerReading);   //digits before decimal point: 2 , after decimal point: 1
  lcd.print(buf);

  lcd.setCursor(7, line);
  if(carbonizerState == 1)
    lcd.print(" ON ");
  else if(carbonizerState == 0)
    lcd.print(" OFF");

  lcd.setCursor(13, line);
  if(carbonizerTrigger == 0)
    lcd.print("Online");
  else if(carbonizerTrigger == 1)
    lcd.print("Schd");
}
// ************************************************************************
void mFunc_para_Ozonizer(uint8_t line)
{
// check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
      }
    }
  }
  char buf[40];
  // use the line from function parameters
  lcd.setCursor(1, line);
//  lcd.print("Ozonizer :");
//  lcd.setCursor(14, line);
//  sprintf (buf, "%s", dtostrf(OzonizerReading, 2, 1, &buf[14]));   //digits before decimal point: 2 , after decimal point: 1
  sprintf (buf, "%d%", ozonizerReading);   //digits before decimal point: 2 , after decimal point: 1
  lcd.print(buf);

  lcd.setCursor(7, line);
  if(ozonizerState == 1)
    lcd.print(" ON ");
  else if(ozonizerState == 0)
    lcd.print(" OFF");

  lcd.setCursor(13, line);
  if(ozonizerTrigger == 0)
    lcd.print("Online");
  else if(ozonizerTrigger == 1)
    lcd.print("Schd");
}
// ************************************************************************
void mFunc_para_Clarifier(uint8_t line)
{
// check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
      }
    }
  }
  char buf[40];
  // use the line from function parameters
  lcd.setCursor(1, line);
//  lcd.print("Clarifier :");
//  lcd.setCursor(14, line);
//  sprintf (buf, "%s", dtostrf(ClarifierReading, 2, 1, &buf[14]));   //digits before decimal point: 2 , after decimal point: 1
  sprintf (buf, "%d%", clarifierReading);   //digits before decimal point: 2 , after decimal point: 1
  lcd.print(buf);

  lcd.setCursor(7, line);
  if(clarifierState == 1)
    lcd.print(" ON ");
  else if(clarifierState == 0)
    lcd.print(" OFF");

  lcd.setCursor(13, line);
  if(clarifierTrigger == 0)
    lcd.print("Online");
  else if(clarifierTrigger == 1)
    lcd.print("Schd");
}
//**************************************************************************
void mFunc_para_Regulator(uint8_t line) {
// check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
      }
    }
  }

}

//**************************************************************************
void mFunc_para_Purifier(uint8_t line) {
// check if this function is active (cursor stands on this line)
  if (line == LCDML.MENU_getCursorPos())
  {
    // make only an action when the cursor stands on this menu item
    //check Button
    if(LCDML.BT_checkAny())
    {
      if(LCDML.BT_checkEnter())
      {
      }
    }
  }

}
//**************************************************************************
void Task_Screensaver() {

static int screensaverPage = 0;

  switch(screensaverPage)
  {
    case 0:
            showClimatizer1Status();
    break;

    case 1:
            showClimatizer2Status();
    break;

    case 2:
            showFertigator1Status();
    break;

    case 3:
            showFertigator2Status();
    break;

    case 4:
            showIrrigatorStatus();
    break;

    case 5:
            showLuminatorStatus();
    break;

    default:
 //           screensaverPage = 0;
    break;
    
  }
  screensaverPage++;
  if(screensaverPage > 5)
  {
    screensaverPage = 0;
  }

  
// showClimatizer1Status();
//  delay(5000);
//  showClimatizer2Status();
//  delay(5000);
//  showFertigator1Status();
//  delay(5000);
//  showFertigator2Status();
//  delay(5000);
//  showIrrigatorStatus();
//  delay(5000);
//  showLuminatorStatus();
//  delay(5000);
}
void showClimatizer1Status()
{
  lcd.clear();
  lcd.setCursor(12,0);
  lcd.write(11);
  lcd.setCursor(15,0);
  lcd.print("Climatizer (1)");
  lcd.setCursor(0,1);
  lcd.print(" Conditioner>");
  lcd.setCursor(13,1);
  if(conditionerState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,1);
  lcd.print("Cur:");
  lcd.setCursor(21,1);
  lcd.print(conditionerReading);
  lcd.setCursor(25,1);
  lcd.print((char) 223);
  lcd.setCursor(26,1);
  lcd.print("C");
  lcd.setCursor(28,1);
  lcd.print("Set:");
  lcd.setCursor(32,1);
  lcd.print(conditionerSetpoint);
  lcd.setCursor(36,1);
  lcd.print((char) 223);
  lcd.setCursor(37,1);
  lcd.print("C");
  
  lcd.setCursor(0,2);
  lcd.print("  Humidifier>");
  lcd.setCursor(13,2);
  if(humidifierState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,2);
  lcd.print("Cur:");
  lcd.setCursor(21,2);
  lcd.print(humidifierReading);
  lcd.setCursor(23,2);
  lcd.print("%");
  lcd.setCursor(28,2);
  lcd.print("Set:");
  lcd.setCursor(32,2);
  lcd.print(humidifierSetpoint);
  lcd.setCursor(34,2);
  lcd.print("%");

  lcd.setCursor(0,3);
  lcd.print("  Ventilator>");
  lcd.setCursor(13,3);
  lcd.print("OFF");
  lcd.setCursor(17,3);
  lcd.print("Cur:");
  lcd.setCursor(21,3);
  lcd.print("75");
  lcd.setCursor(23,3);
  lcd.print("cfm");
  lcd.setCursor(28,3);
  lcd.print("Set:");
  lcd.setCursor(32,3);
  lcd.print("55");
  lcd.setCursor(34,3);
  lcd.print("cfm");
 }

void showClimatizer2Status()
{
  lcd.clear();
  lcd.setCursor(12,0);
  lcd.write(11);
  lcd.setCursor(15,0);
  lcd.print("Climatizer (2)");

  lcd.setCursor(0,1);
  lcd.print("  Carbonizer>");
  lcd.setCursor(13,1);
  if(carbonizerState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,1);
  lcd.print("Cur:");
  lcd.setCursor(21,1);
  lcd.print(carbonizerReading);
  lcd.setCursor(23,1);
  lcd.print("ppm ");
  lcd.setCursor(28,1);
  lcd.print("Set:");
  lcd.setCursor(32,1);
  lcd.print(carbonizerSetpoint);
  lcd.setCursor(34,1);
  lcd.print("ppm ");
  
  lcd.setCursor(0,2);
  lcd.print("    Ozonizer>");
  lcd.setCursor(13,2);
  if(ozonizerState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,2);
  lcd.print("Cur:");
  lcd.setCursor(21,2);
  lcd.print(ozonizerReading);
  lcd.setCursor(23,2);
  lcd.print("ppm ");
  lcd.setCursor(28,2);
  lcd.print("Set:");
  lcd.setCursor(32,2);
  lcd.print(ozonizerSetpoint);
  lcd.setCursor(34,2);
  lcd.print("ppm ");

  lcd.setCursor(0,3);
  lcd.print("   Clarifier>");
  lcd.setCursor(13,3);
  if(clarifierState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,3);
  lcd.print("Cur:");
  lcd.setCursor(21,3);
  lcd.print(clarifierReading);
  lcd.setCursor(23,3);
  lcd.print("ppm");
  lcd.setCursor(28,3);
  lcd.print("Set:");
  lcd.setCursor(32,3);
  lcd.print(clarifierSetpoint);
  lcd.setCursor(34,3);
  lcd.print("ppm");
 }
void showFertigator1Status()
{
  lcd.clear();
  lcd.setCursor(12,0);
  lcd.write(12);
  lcd.setCursor(15,0);
  lcd.print("Fertigator (1)");
  lcd.setCursor(26,0);

  lcd.setCursor(0,1);
  lcd.print("    Balancer>");
  lcd.setCursor(13,1);
  lcd.print("OFF");
//  if(balancerState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,1);
  lcd.print("Cur:");
  lcd.setCursor(21,1);
  lcd.print("9.00");
//  lcd.print(balancerReading);
  lcd.setCursor(25,1);
  lcd.print("   ");
  lcd.setCursor(28,1);
  lcd.print("Set:");
  lcd.setCursor(32,1);
  lcd.print("5.80");
//  lcd.print(balancerSetpoint);
  lcd.setCursor(37,1);
  lcd.print("   ");

  lcd.setCursor(0,2);
  lcd.print("Supplementer>");
  lcd.setCursor(13,2);
  lcd.print("OFF");
//  if(supplementerState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,2);
  lcd.print("Cur:");
  lcd.setCursor(21,2);
  lcd.print("1.10");
//  lcd.print(supplementerReading);
  lcd.setCursor(25,2);
  lcd.print(" ");
  lcd.setCursor(28,2);
  lcd.print("Set:");
  lcd.setCursor(32,2);
  lcd.print("2.20");
//  lcd.print(supplementerSetpoint);
  lcd.setCursor(37,2);
  lcd.print(" ");

  lcd.setCursor(0,3);
  lcd.print("   Regulator>");
  lcd.setCursor(13,3);
  if(regulatorState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,3);
  lcd.print("Cur:");
  lcd.setCursor(21,3);
  lcd.print(regulatorReading);
  lcd.setCursor(23,3);
  lcd.print((char) 223);
  lcd.setCursor(24,3);
  lcd.print("C");
  lcd.setCursor(28,3);
  lcd.print("Set:");
  lcd.setCursor(32,3);
  lcd.print(regulatorSetpoint);
  lcd.setCursor(34,3);
  lcd.print((char) 223);
  lcd.setCursor(35,3);
  lcd.print("C");
  
  }

void showFertigator2Status()
{
  lcd.clear();
  lcd.setCursor(12,0);
  lcd.write(12);
  lcd.setCursor(15,0);
  lcd.print("Fertigator (2)");
  lcd.setCursor(26,0);

  lcd.setCursor(0,1);
  lcd.print("     Chamber>");
  lcd.setCursor(13,1);
  lcd.print("OFF");
//  if(chamberState == 0)lcd.print("OFF ");else lcd.print("ON  ");  //check pump state
  lcd.setCursor(17,1);
  lcd.print("Cur:");
  lcd.setCursor(21,1);
  lcd.print(chamberLevelReading);
  lcd.setCursor(24,1);
  lcd.print("ltr");
  lcd.setCursor(28,1);
  lcd.print("Set:");
  lcd.setCursor(32,1);
  lcd.print("456");   //set point???
  lcd.setCursor(35,1);
  lcd.print("ltr");

  lcd.setCursor(0,2);
  lcd.print("    Supplier>");
  lcd.setCursor(13,2);
//  if(supplierState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,2);
  lcd.print("Cur:");
  lcd.setCursor(21,2);
  lcd.print("121");
  lcd.setCursor(24,2);
  lcd.print("ltr");
  lcd.setCursor(28,2);
  lcd.print("Set:");
  lcd.setCursor(32,2);
  lcd.print("222");
  lcd.setCursor(35,2);
  lcd.print("ltr");

  lcd.setCursor(0,3);
  lcd.print("     Aerator>");
  lcd.setCursor(13,3);
  if(aeratorState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,3);
  lcd.print("Cur:");
  lcd.setCursor(21,3);
  lcd.print("10");    //reading??
  lcd.setCursor(23,3);
  lcd.print("mgl");
  lcd.setCursor(28,3);
  lcd.print("Set:");
  lcd.setCursor(32,3);
  lcd.print("7");     //setpoint??
  lcd.setCursor(33,3);
  lcd.print("mgl");
  
  }

void showIrrigatorStatus()
{
  lcd.clear();
  lcd.setCursor(12,0);
  lcd.write(13);
  lcd.setCursor(15,0);
  lcd.print("Irrigator (1)");
  lcd.setCursor(26,0);

  lcd.setCursor(0,1);
  lcd.print("   Reservoir>");
  lcd.setCursor(13,1);
  lcd.print("OFF");
//  if(reservoirState == 0)lcd.print("OFF ");else lcd.print("ON  ");  //pump state
  lcd.setCursor(17,1);
  lcd.print("Cur:");
  lcd.setCursor(21,1);
  lcd.print(reservoirLevelReading);
  lcd.setCursor(25,1);
  lcd.print("ltr");
  lcd.setCursor(28,1);
  lcd.print("Set:");
  lcd.setCursor(32,1);
  lcd.print("299");   //setpoint ???
  lcd.setCursor(37,1);
  lcd.print("ltr");

  lcd.setCursor(0,2);
  lcd.print("    Purifier>");
  lcd.setCursor(13,2);
  if(purifierState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,2);
  lcd.print("Cur:");
  lcd.setCursor(21,2);
  lcd.print(purifierReading);
  lcd.setCursor(24,2);
  lcd.print(" ");
  lcd.setCursor(28,2);
  lcd.print("Set:");
  lcd.setCursor(32,2);
  lcd.print(purifierSetpoint);
  lcd.setCursor(36,2);
  lcd.print(" ");

  lcd.setCursor(0,3);
  lcd.print("    Recycler>");
  lcd.setCursor(13,3);
  lcd.print("OFF");
//  if(recyclerState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,3);
  lcd.print("Cur:");
  lcd.setCursor(21,3);
  lcd.print("23");    //reading??
  lcd.setCursor(23,3);
  lcd.print((char) 223);
  lcd.setCursor(24,3);
  lcd.print("C");
  lcd.setCursor(28,3);
  lcd.print("Set:");
  lcd.setCursor(32,3);
  lcd.print("25");    //setpoint??
  lcd.setCursor(34,3);
  lcd.print((char) 223);
  lcd.setCursor(35,3);
  lcd.print("C");
  
  }

  void showLuminatorStatus()
{
  lcd.clear();
  lcd.setCursor(12,0);
  lcd.write(14);
  lcd.setCursor(15,0);
  lcd.print("Luminator (1)");
  lcd.setCursor(26,0);

  lcd.setCursor(0,1);
  lcd.print("   Duration>");
  lcd.setCursor(13,1);
  lcd.print("OFF");
//  if(luminatorState == 0)lcd.print("OFF ");else lcd.print("ON  ");
  lcd.setCursor(17,1);
  lcd.print("Cur:");
  lcd.setCursor(21,1);
  lcd.print("12");    //reading
  lcd.setCursor(23,1);
  lcd.print("Hrs");
  lcd.setCursor(28,1);
  lcd.print("Set:");
  lcd.setCursor(32,1);
  lcd.print("12");    //setpoint
  lcd.setCursor(36,1);
  lcd.print("Hrs");

  lcd.setCursor(0,2);
  lcd.print("    Spectrum>");
  lcd.setCursor(13,2);
  lcd.print("OFF");
  lcd.setCursor(17,2);
  lcd.print("Cur:");
  lcd.setCursor(21,2);
  lcd.print("400");
  lcd.setCursor(24,2);
  lcd.print("nm");
  lcd.setCursor(28,2);
  lcd.print("Set:");
  lcd.setCursor(32,2);
  lcd.print("600");
  lcd.setCursor(36,2);
  lcd.print("nm");

  lcd.setCursor(0,3);
  lcd.print("    Intensity>");
  lcd.setCursor(13,3);
  lcd.print("OFF");
  lcd.setCursor(17,3);
  lcd.print("Cur:");
  lcd.setCursor(21,3);
  lcd.print("23");
  lcd.setCursor(23,3);
  lcd.print((char) 223);
  lcd.setCursor(24,3);
  lcd.print("C");
  lcd.setCursor(28,3);
  lcd.print("Set:");
  lcd.setCursor(32,3);
  lcd.print("25");
  lcd.setCursor(34,3);
  lcd.print((char) 223);
  lcd.setCursor(35,3);
  lcd.print("C");
  
  }

  
