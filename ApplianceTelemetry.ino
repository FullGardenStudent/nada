
//char publishSensorsJsonBuffer[256];
//char publishControllersJsonBuffer[256];

const size_t deviceTelemetryDocCapacity = JSON_OBJECT_SIZE(60);
DynamicJsonDocument deviceTelemetryDoc(deviceTelemetryDocCapacity);
JsonArray nestedObjConditioner   = deviceTelemetryDoc.createNestedArray("CON");
JsonArray nestedObjHumidifier    = deviceTelemetryDoc.createNestedArray("HUM");  
JsonArray nestedObjCarbonizer    = deviceTelemetryDoc.createNestedArray("CAR");  
JsonArray nestedObjOzonizer      = deviceTelemetryDoc.createNestedArray("OZO");  
JsonArray nestedObjClarifier     = deviceTelemetryDoc.createNestedArray("CLA"); 
JsonArray nestedObjVentilator    = deviceTelemetryDoc.createNestedArray("VEN");  //22
 
JsonArray nestedObjReservoir     = deviceTelemetryDoc.createNestedArray("RES");
JsonArray nestedObjSterilizer    = deviceTelemetryDoc.createNestedArray("STE");  //30
JsonArray nestedObjPurifier      = deviceTelemetryDoc.createNestedArray("PUR");
JsonArray nestedObjRecycler      = deviceTelemetryDoc.createNestedArray("REC");  //34

JsonArray nestedObjChamber       = deviceTelemetryDoc.createNestedArray("CHA");
JsonArray nestedObjRegulator     = deviceTelemetryDoc.createNestedArray("REG");
JsonArray nestedObjAerator       = deviceTelemetryDoc.createNestedArray("AER");
JsonArray nestedObjMixer         = deviceTelemetryDoc.createNestedArray("MIX");
JsonArray nestedObjCirculator    = deviceTelemetryDoc.createNestedArray("CIR");
JsonArray nestedObjBalancer      = deviceTelemetryDoc.createNestedArray("BAL");
JsonArray nestedObjSupplementer  = deviceTelemetryDoc.createNestedArray("SUP");

JsonArray nestedObjLuminator     = deviceTelemetryDoc.createNestedArray("LUM");
JsonArray nestedObjHarmonizer    = deviceTelemetryDoc.createNestedArray("HAR");

static char buffer[512];


char* receiveApplianceTelemetry(void)
{

//  jsonSubscribeDoc["node"] = "OzoneGenerator";
//  jsonSubscribeDoc["title"] = "Ozone Generator";
//  jsonSubscribeDoc["value"] = ozonizerReading;  //sensorValue;
//  jsonSubscribeDoc["type"] = "number";
//  jsonSubscribeDoc["unit"] = "ppm";
//  jsonSubscribeDoc["readOnly"] = "true";
//  jsonSubscribeDoc["description"] = "Ozone(O3) sensor";
//  serializeJson(jsonSubscribeDoc, Serial);
//  serializeJson(jsonSubscribeDoc, buffer) ;
//  return buffer;
}
//deviceTelemetryDoc
//deviceStateDoc
//deviceTriggerDoc
//growingRecipeCalenderDoc
//growingRecipeDailyDoc

void buildApplianceTelemetry(void)
{
  nestedObjConditioner.add(roundDecimalPoint(conditionerReading,1));
  nestedObjConditioner.add(conditionerState);

  nestedObjHumidifier.add(roundDecimalPoint(humidifierReading,1));  
  nestedObjHumidifier.add(humidifierState);

  nestedObjCarbonizer.add(64);  //(carbonizerReading);  
  nestedObjCarbonizer.add(carbonizerState);

//  nestedObjOzonizer.add(ozonizerReading);  
//  nestedObjOzonizer.add(ozonizerState); 

  nestedObjClarifier.add(139); //(clarifierReading);    //air quality
  nestedObjClarifier.add(clarifierState); 

  nestedObjVentilator.add(100);                //air flow
  nestedObjVentilator.add(ventilatorState); 

  nestedObjReservoir.add(reservoirLevelReading);      //level
  nestedObjReservoir.add(20);                         //Volume
  nestedObjReservoir.add(0);    //pump state reservoirState;  

  nestedObjSterilizer.add(sterilizerReading);        //Turbidity
  nestedObjSterilizer.add(0);

  nestedObjPurifier.add(0);

  nestedObjRecycler.add(0);

  nestedObjChamber.add(chamberLevelReading);  
  nestedObjChamber.add(10);  //chamberVolumeReading;  
  nestedObjChamber.add(0);

  nestedObjRegulator.add(regulatorReading);  //water temperature
  nestedObjRegulator.add(regulatorState);

  nestedObjAerator.add(0);  
  
  nestedObjMixer.add(0);  

  nestedObjCirculator.add(0);      //circulatorReading //volume 
  nestedObjCirculator.add(0);  

  nestedObjBalancer.add(1.1);       //balancerReading;  
  nestedObjBalancer.add(0);           //balancerState;  

  nestedObjSupplementer.add(12.1);      //supplementerReading;  
  nestedObjSupplementer.add(0);           //supplementerState;  

  nestedObjLuminator.add(10);      //luminatorReading;    //intensity
  nestedObjLuminator.add(102);      //luminatorReading;    //spectrum
  nestedObjLuminator.add(102);      //luminatorReading;  
  nestedObjLuminator.add(0);           //luminatorState;  

  nestedObjHarmonizer.add(102);      //harmonizerReading;  //Intensity
  nestedObjHarmonizer.add(102);      //harmonizerReading;  //spectrum
  nestedObjHarmonizer.add(0);           //harmonizerState;  

}

//  nestedObjConditioner["Setpoint"] = conditionerSetpoint; 
// nestedObjConditioner["U"] = "C";
//  if(conditionerUnits == 0) nestedObjConditioner["U"] = "C"; 
//  else nestedObjConditioner["U"] = "F";
//  if(conditionerState == 1) nestedObjConditioner["state"] = "ON"; //conditionerState; 
//  else nestedObjConditioner["State"] = "OFF";
  //nestedObjConditioner["trigger"] = conditionerTrigger;  
  
//  nestedObjHumidifier["Setpoint"] = humidifierSetpoint;  
//  nestedObjHumidifier["U"] = "%";                   //humidifierUnits;  
//  if(humidifierState == 1) nestedObjHumidifier["State"] = "ON"; //humidifierState; 
//  else nestedObjHumidifier["State"] = "OFF";
  //nestedObjHumidifier["trigger"] = humidifierTrigger;
  
//  nestedObjCarbonizer["Setpoint"] = carbonizerSetpoint;  
//  nestedObjCarbonizer["U"] = "ppm";                //carbonizerUnits;  
//  if(carbonizerState == 1) nestedObjCarbonizer["State"] = "ON"; //carbonizerState; 
//  else nestedObjCarbonizer["State"] = "OFF";
  //nestedObjCarbonizer["trigger"] = carbonizerTrigger;  
  
//  nestedObjOzonizer["Setpoint"] = ozonizerSetpoint;  
//  nestedObjOzonizer["U"] = "ppm";  
//  if(ozonizerState == 1) nestedObjOzonizer["State"] = "ON"; //ozonizerState; 
//  else nestedObjOzonizer["State"] = "OFF";
  //nestedObjOzonizer["trigger"] = ozonizerTrigger; 
  
//  nestedObjClarifier["Setpoint"] = clarifierSetpoint;  
//  nestedObjClarifier["U"] = "ppm";                 //clarifierUnits;  
//  if(clarifierState == 1) nestedObjClarifier["State"] = "ON"; //clarifierState; 
//  else nestedObjClarifier["State"] = "OFF";
  //nestedObjClarifier["trigger"] = clarifierTrigger; 
  
//  if(ventilatorState == 1) nestedObjVentilator["State"] = "ON"; 
//  nestedObjVentilator["State"] = "OFF"; 
  //nestedObjVentilator["trigger"] = ventilatorTrigger; 
  
//  nestedObjReservoir["Level"] = reservoirLevelReading;  
//  //nestedObjReservoir["Setpoint"] = reservoirSetpoint;  
////  nestedObjReservoir["U"] = "%";  
//  //nestedObjReservoir["Volume"] = ;  
//  //nestedObjReservoir["Unit"] = "ltr";  
//  //if(reservoirState == 1) nestedObjReservoir["State"] = "ON"; //reservoirState; 
//  //else nestedObjReservoir["State"] = "OFF";
//  nestedObjReservoir["S"] = 0;    //pump state reservoirState;  
//  //nestedObjReservoir["trigger"] = reservoirTrigger; 
//  
//  nestedObjSterilizer["Turbidity"] = sterilizerReading;  
////  nestedObjSterilizer["U"] = " ";  
//  nestedObjSterilizer["S"] = 0;
////  if(sterilizerState == 1) nestedObjSterilizer["State"] = "ON"; 
////  else nestedObjSterilizer["State"] = "OFF";
//  //nestedObjSterilizer["trigger"] = sterilizerTrigger; 
//
//  nestedObjPurifier["S"] = 0;
////  if(purifierState == 1) nestedObjPurifier["State"] = "ON"; 
////  else nestedObjPurifier["State"] = "OFF";
//  //nestedObjPurifier["trigger"] = purifierTrigger; 
//
//  nestedObjRecycler["S"] = 0;
////  if(recyclerState == 1) nestedObjRecycler["State"] = "ON"; 
////  else nestedObjRecycler["State"] = "OFF";
//  //nestedObjRecycler["trigger"] = purifierRecycler; 
//  
//  nestedObjChamber["Level"] = chamberLevelReading;  
//  //nestedObjChamber["Setpoint"] = chamberSetpoint;  
////  nestedObjChamber["U"] = "%";
//  //nestedObjChamber["Volume"] = chamberVolumeReading;  
//  //nestedObjChamber["Unit"] = "ltr";
//  nestedObjChamber["S"] = 0;
////  if(chamberState == 1)   nestedObjChamber["State"] = "ON";  
////  nestedObjChamber["State"] = "OFF";    //pump state  chamberState;  
//  //nestedObjChamber["trigger"] = chamberTrigger; 
//  
//  nestedObjRegulator["Water temperature"] = regulatorReading;  
////  nestedObjRegulator["Setpoint"] = regulatorSetpoint;  
////  nestedObjRegulator["U"] = "centigrade";                  //regulatorUnits;  
//  nestedObjRegulator["S"] = regulatorState;
////  if(regulatorState == 1) nestedObjRegulator["State"] = "ON"; //regulatorState; 
////  else nestedObjRegulator["State"] = "OFF";
//  //nestedObjRegulator["trigger"] = regulatorTrigger; 
//  
////  if(aeratorState == 1)   nestedObjAerator["State"] = "ON";
//  nestedObjAerator["S"] = 0;  
//  
////  if(mixerState == 1)   nestedObjMixer["State"] = "ON";
//  nestedObjMixer["S"] = 0;  
  
////  if(CirculatorState == 1)   nestedObjCirculator["State"] = "ON";
//  nestedObjCirculator["S"] = 0;  
//  
//  
//  nestedObjBalancer["pH"] = 1.1;       //balancerReading;  
////  nestedObjBalancer["Setpoint"] = 1.1;      //balancerSetpoint;  
//  nestedObjBalancer["U"] = " ";  
////  if(balancerState == 1) nestedObjBalancer["State"] = "ON";
//  nestedObjBalancer["S"] = 0;           //balancerState;  
//  //nestedObjBalancer["trigger"] = balancerTrigger; 
//  
//  nestedObjSupplementer["Ec"] = 12.1;      //supplementerReading;  
////  nestedObjSupplementer["Setpoint"] = 12.3;     //supplementerSetpoint;  
//  nestedObjSupplementer["U"] = " ";   //supplementerUnits;  
////  if(supplementerState == 1) nestedObjSupplementer["State"] = "ON";
//  nestedObjSupplementer["S"] = 0;           //supplementerState;  
//  //nestedObjSupplementer["trigger"] = supplementerTrigger; 
//  
//  nestedObjLuminator["Intensity"] = 102;      //luminatorReading;  
////  nestedObjLuminator["Setpoint"] = 120;     //luminatorSetpoint;  
//  nestedObjLuminator["U"] = " ";         //luminatorUnits;  
//  nestedObjLuminator["Spectrum"] = 102;      //luminatorReading;  
////  nestedObjLuminator["Setpoint"] = 120;     //luminatorSetpoint;  
//  nestedObjLuminator["U"] = "nm";         //luminatorUnits;  
////  if(luminatorState == 1) nestedObjLuminator["State"] = "ON";
//  nestedObjLuminator["S"] = 0;           //luminatorState;  
//  //nestedObjLuminator["trigger"] = luminatorTrigger; 
//  
//  nestedObjHarmonizer["Spectrum"] = 102;      //harmonizerReading;  
////  nestedObjHarmonizer["Setpoint"] = 120;     //harmonizerSetpoint;  
//  nestedObjHarmonizer["U"] = " ";         //harmonizerUnits;  
////  if(harmonizerState == 1) nestedObjHarmonizer["State"] = "ON";
//  nestedObjHarmonizer["S"] = 0;           //harmonizerState;  
////  //nestedObjHarmonizer["trigger"] = harmonizerTrigger; 

//data units????
//increase publishDocCapacity size for publishing below topics

void buildApplianceStateDoc(void) {
//  deviceStateDoc["Conditioner State"] = conditionerState; 
//  deviceStateDoc["Humidifier State"] = humidifierState;
//  deviceStateDoc["Carbonizer State"] = carbonizerState;
//  deviceStateDoc["Ozonizer State"] = ozonizerState;
//  deviceStateDoc["Clarifier State"] = clarifierState; 

//  deviceStateDoc["Regulator State"] = regulatorState; 
//  deviceStateDoc["Irrigator State"] = irrigatorState; 
//  deviceStateDoc["Purifier State"] = purifierState; 
//  deviceStateDoc["Aerator State"] = aeratorState; 
//  deviceStateDoc["Mixer State"] = mixerState; 
//  deviceStateDoc["Sterilizer State"] = sterilizerState; 
}
void buildApplianceSetpointDoc(void) {   
//  deviceSetpointDoc["Conditioner Setpoint"] = conditionerSetpoint; 
//  deviceSetpointDoc["Humidifier Setpoint"] = humidifierSetpoint;
//  deviceSetpointDoc["Carbonizer Setpoint"] = carbonizerSetpoint;
//  deviceSetpointDoc["Ozonizer Setpoint"] = ozonizerSetpoint;
//  deviceSetpointDoc["Clarifier Setpoint"] = clarifierSetpoint; 

//  deviceSetpointDoc["Regulator Setpoint"] = regulatorSetpoint; 
//  deviceSetpointDoc["Irrigator Setpoint"] = irrigatorSetpoint; 
//  deviceSetpointDoc["Purifier Setpoint"] = purifierSetpoint; 
//  deviceSetpointDoc["Aerator Setpoint"] = aeratorSetpoint; 
//  deviceSetpointDoc["Mixer Setpoint"] = mixerSetpoint; 
//  deviceSetpointDoc["Sterilizer Setpoint"] = sterilizerSetpoint; 
}
void buildApplianceTriggerDoc(void) {
//  deviceTriggerDoc["Conditioner Trigger"] = conditionerTrigger; 
//  deviceTriggerDoc["Humidifier Trigger"] = humidifierTrigger;
//  deviceTriggerDoc["Carbonizer Trigger"] = carbonizerTrigger;
//  deviceTriggerDoc["Ozonizer Trigger"] = ozonizerTrigger;
//  deviceTriggerDoc["Clarifier Trigger"] = clarifierTrigger; 

//  deviceTriggerDoc["Regulator Trigger"] = regulatorTrigger; 
//  deviceTriggerDoc["Irrigator Trigger"] = irrigatorTrigger; 
//  deviceTriggerDoc["Purifier Trigger"] = purifierTrigger; 
//  deviceTriggerDoc["Aerator Trigger"] = aeratorTrigger; 
//  deviceTriggerDoc["Mixer Trigger"] = mixerTrigger; 
//  deviceTriggerDoc["Sterilizer Trigger"] = sterilizerTrigger; 
}

void sendMQTTTelemetry() {
  
//  size_t n = serializeJson(deviceTelemetryDoc, buffer);  
//  psClient.publish(APPLIANCE_TELEMETRY, buffer, n); // Publish topic and buffer
}

void sendHTTPTelemetry() {

  serializeJson(deviceTelemetryDoc, Serial);    //to serial monitor
  serializeJson(deviceTelemetryDoc, buffer);
  String content = buffer;
  Serial.println("Content-Length: " + String(content.length()));    //to serial monitor
  
  client.println("POST /api/sendsignal?code=/ahskyY7D2UCPXxPVbUZ056Tny/iIcNW0dB1S83d1GX667ZRELWq7g==&deviceid=202 HTTP/1.1");
  client.println("Host: cenauraiotpoc.azurewebsites.net");
  client.println("Accept: /");
  client.println("Content-Length: " + String(content.length()));
  client.println("Content-Type: application/json");
  client.println();
  client.println(content);  
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(built_in_led, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is acive low on the ESP-01)
  } else {
    digitalWrite(built_in_led, HIGH);  // Turn the LED off by making the voltage HIGH
  }
}

void reconnect() {
  // Loop until we're reconnected
//  while (!psClient.connected()) {
//    Serial.print("Attempting MQTT connection...");
//    // Attempt to connect
//    if (psClient.connect("ESP8266Client")) {
////if (psClient.connect("pgwemos","cenaura-cloud-hub-test.azure-devices.net/pgwemos/?api-version=2018-06-30","SharedAccessSignature sr=cenaura-cloud-hub-test.azure-devices.net%2Fdevices%2Fpgwemos&sig=dB8L6hAHx5Duoc7bIydHfgdx32%2FD8S6nEMRuzN%2FIe2E%3D&se=1761089475")) {
//      Serial.println("connected");
//      // Once connected, publish an announcement...
//      psClient.publish("outTopic", "hello world");
//      
//      // ... and resubscribe
//      psClient.subscribe("inTopic");
//
//    } else {
//      Serial.print("failed, rc=");
//      Serial.print(psClient.state());
//      Serial.println(" try again in 5 seconds");
//      // Wait 5 seconds before retrying
//      delay(5000);
//    }
//  }
}

float roundDecimalPoint( float in_value, int decimal_place )
{
  float multiplier = powf( 10.0f, decimal_place );
  in_value = roundf( in_value * multiplier ) / multiplier;
  return in_value;
}
