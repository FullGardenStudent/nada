
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
//  Serial.println();
//  Serial.print("Connecting to ");
//  Serial.println(ssid);
//
//  WiFi.begin(ssid, password);

 // check for the presence of the shield
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue
 //   while (true);
    flagWifiConnected = 0;
  }

  // attempt to connect to WiFi network
//  while ( status != WL_CONNECTED) {
  if( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ssid, password);
  }

  if(status == WL_CONNECTED)
  {
    flagWifiConnected = 1;
  // you're connected now, so print out the data
   Serial.println("You're connected to the network");

//  printWifiStatus();

    Serial.println();
    Serial.println("Starting connection to Azure server...");
    // if you get a connection, report back via serial
    if (client.connect(server, 80)) {
      Serial.println("Connected to Azure server");
    }

  }
//
//  while (WiFi.status() != WL_CONNECTED) {
//    delay(500);
//    Serial.print(".");
//  }
//
//  Serial.println("");
//  Serial.println("WiFi connected");
//  Serial.println("IP address: ");
//  Serial.println(WiFi.localIP());
}







void sendHttpResponse(WiFiEspClient espClient)
{
  // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
  // and a content-type so the client knows what's coming, then a blank line:
  espClient.println("HTTP/1.1 200 OK");
  espClient.println("Content-type:text/html");
  espClient.println();
  
  // the content of the HTTP response follows the header:
  espClient.print("The LED is ");
  espClient.print(ledStatus);
  espClient.println("<br>");
  espClient.println("<br>");
  
  espClient.println("Click <a href=\"/H\">here</a> turn the LED on<br>");
  espClient.println("Click <a href=\"/L\">here</a> turn the LED off<br>");
  
  // The HTTP response ends with another blank line:
  espClient.println();
}

void processHttpRequest(){
//  WiFiEspClient espClient = server.available();  // listen for incoming clients
//
//  if (espClient) {                               // if you get a client,
//    Serial.println("New client");             // print a message out the serial port
//    buf.init();                               // initialize the circular buffer
//    while (espClient.connected()) {              // loop while the client's connected
//      if (espClient.available()) {               // if there's bytes to read from the client,
//        char c = espClient.read();               // read a byte, then
//        buf.push(c);                          // push it to the ring buffer
//
//        // printing the stream to the serial monitor will slow down
//        // the receiving of data from the ESP filling the serial buffer
//        //Serial.write(c);
//        
//        // you got two newline characters in a row
//        // that's the end of the HTTP request, so send a response
//        if (buf.endsWith("\r\n\r\n")) {
//          sendHttpResponse(espClient);
//          break;
//        }
//
//        // Check to see if the client request was "GET /H" or "GET /L":
//        if (buf.endsWith("GET /H")) {
//          Serial.println("Turn led ON");
//          ledStatus = HIGH;
//          digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
//        }
//        else if (buf.endsWith("GET /L")) {
//          Serial.println("Turn led OFF");
//          ledStatus = LOW;
//          digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
//        }
//      }
//    }
//    
//    // close the connection
//    espClient.stop();
//    Serial.println("Client disconnected");
//  }
}
