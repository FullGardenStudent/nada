//**********************************************************************
//EEPROM
//**********************************************************************
const int maxAllowedWrites = 80;
const int memBase          = 350;

int addressConditionerSetpoint;
int addressConditionerUnits;
int addressConditionerTrigger;
int addressConditionerSchdOffMode;
int addressConditionerSchdOnTime;
int addressConditionerSchdOffTime;

int addressHumidifierSetpoint;
int addressHumidifierTrigger;
int addressHumidifierSchdOffMode;
int addressHumidifierSchdOnTime;
int addressHumidifierSchdOffTime;

int addressCarbonizerSetpoint;
int addressCarbonizerTrigger;
int addressCarbonizerSchdOffMode;
int addressCarbonizerSchdOnTime;
int addressCarbonizerSchdOffTime;

int addressOzonizerSetpoint;
int addressOzonizerTrigger;
int addressOzonizerSchdOffMode;
int addressOzonizerSchdOnTime;
int addressOzonizerSchdOffTime;

int addressClarifierSetpoint;
int addressClarifierTrigger;
int addressClarifierSchdOffMode;
int addressClarifierSchdOnTime;
int addressClarifierSchdOffTime;

void setup_EEPROM() {
  // start reading from position memBase (address 0) of the EEPROM. Set maximumSize to EEPROMSizeUno 
  // Writes before membase or beyond EEPROMSizeUno will only give errors when _EEPROMEX_DEBUG is set
//  EEPROM.setMemPool(memBase, EEPROMSizeUno);
  EEPROM.setMemPool(memBase, EEPROMSizeMega);   //Set maximumSize to EEPROMSizeMega - 4096
  
  // Set maximum allowed writes to maxAllowedWrites. 
  // More writes will only give errors when _EEPROMEX_DEBUG is set
  EEPROM.setMaxAllowedWrites(maxAllowedWrites);
  delay(100);
//********************************************************************
// Always get the adresses first and in the same order
  addressConditionerUnits         = EEPROM.getAddress(sizeof(byte));
  addressConditionerSetpoint      = EEPROM.getAddress(sizeof(float));
  addressConditionerTrigger       = EEPROM.getAddress(sizeof(byte));
  addressConditionerSchdOffMode   = EEPROM.getAddress(sizeof(byte));
  addressConditionerSchdOnTime    = EEPROM.getAddress(sizeof(byte)*2);
  addressConditionerSchdOffTime   = EEPROM.getAddress(sizeof(byte)*2);

  addressHumidifierSetpoint      = EEPROM.getAddress(sizeof(int));
  addressHumidifierTrigger       = EEPROM.getAddress(sizeof(byte));
  addressHumidifierSchdOffMode   = EEPROM.getAddress(sizeof(byte));
  addressHumidifierSchdOnTime    = EEPROM.getAddress(sizeof(byte)*2);
  addressHumidifierSchdOffTime   = EEPROM.getAddress(sizeof(byte)*2);

  addressCarbonizerSetpoint      = EEPROM.getAddress(sizeof(int));
  addressCarbonizerTrigger       = EEPROM.getAddress(sizeof(byte));
  addressCarbonizerSchdOffMode   = EEPROM.getAddress(sizeof(byte));
  addressCarbonizerSchdOnTime    = EEPROM.getAddress(sizeof(byte)*2);
  addressCarbonizerSchdOffTime   = EEPROM.getAddress(sizeof(byte)*2);
  
  addressOzonizerSetpoint      = EEPROM.getAddress(sizeof(int));
  addressOzonizerTrigger       = EEPROM.getAddress(sizeof(byte));
  addressOzonizerSchdOffMode   = EEPROM.getAddress(sizeof(byte));
  addressOzonizerSchdOnTime    = EEPROM.getAddress(sizeof(byte)*2);
  addressOzonizerSchdOffTime   = EEPROM.getAddress(sizeof(byte)*2);
  
  addressClarifierSetpoint      = EEPROM.getAddress(sizeof(int));
  addressClarifierTrigger       = EEPROM.getAddress(sizeof(byte));
  addressClarifierSchdOffMode   = EEPROM.getAddress(sizeof(byte));
  addressClarifierSchdOnTime    = EEPROM.getAddress(sizeof(byte)*2);
  addressClarifierSchdOffTime   = EEPROM.getAddress(sizeof(byte)*2);
//********************************************************************
  
//Read the EEPROM for settings
  conditionerUnits    = EEPROM.read(addressConditionerUnits);
  if(conditionerUnits > 1) {
    conditionerUnits = 0;
    EEPROM.write(addressConditionerUnits,conditionerUnits);
    conditionerUnits    = EEPROM.read(addressConditionerUnits);
  }
  g_dynParam_ConditionerUnits = conditionerUnits;
  
  conditionerSetpoint = EEPROM.readFloat(addressConditionerSetpoint);
  if( (conditionerSetpoint < CONDITIONER_SETPOINT_MIN) || (conditionerSetpoint > CONDITIONER_SETPOINT_MAX) ) {
    conditionerSetpoint = CONDITIONER_SETPOINT_DEFAULT; //If the value read from EEPROM is out of range, set to default value
    EEPROM.writeFloat(addressConditionerSetpoint,conditionerSetpoint);
    conditionerSetpoint = EEPROM.readFloat(addressConditionerSetpoint);
  }
  g_dynParam_ConditionerSetpoint = conditionerSetpoint;

  conditionerTrigger    = EEPROM.read(addressConditionerTrigger);
  if(conditionerTrigger > 1) {
    conditionerTrigger = ONLINE_TRIGGER;
    EEPROM.write(addressConditionerTrigger,conditionerTrigger);
    conditionerTrigger    = EEPROM.read(addressConditionerTrigger);
  }
  g_dynParam_ConditionerTrigger = conditionerTrigger;

  conditionerSchdOffMode    = EEPROM.read(addressConditionerSchdOffMode);
  if(conditionerSchdOffMode > 1) {
    conditionerSchdOffMode = SCHD_OFF_MODE_AT_VALUE;
    EEPROM.write(addressConditionerSchdOffMode,conditionerSchdOffMode);
    conditionerSchdOffMode    = EEPROM.read(addressConditionerSchdOffMode);
  }
  g_dynParam_ConditionerSchdOffMode = conditionerSchdOffMode;

   EEPROM.readBlock<byte>(addressConditionerSchdOnTime, conditionerSchdOnTime, 2);
//  EEPROM.writeBlock<byte>(addressConditionerSchdOnTime, initial, 2);
//  int writtenBytes = EEPROM.updateBlock<byte>(addressByteArray, input, itemsInArray);
//  g_dynParam_ConditionerSchdOnTime[0] = conditionerSchdOnTime[0];
//  g_dynParam_ConditionerSchdOnTime[1] = conditionerSchdOnTime[1];

  EEPROM.readBlock<byte>(addressConditionerSchdOffTime, conditionerSchdOffTime, 2);
//  EEPROM.writeBlock<byte>(addressConditionerSchdOffTime, initial, 2);
//  int writtenBytes = EEPROM.updateBlock<byte>(addressByteArray, input, itemsInArray);
//  g_dynParam_ConditionerSchdOffTime[0] = conditionerSchdOffTime[0];
//  g_dynParam_ConditionerSchdOffTime[1] = conditionerSchdOffTime[1];  

//******************************************************************************

  humidifierSetpoint = EEPROM.readInt(addressHumidifierSetpoint);
  if( (humidifierSetpoint < HUMIDIFIER_SETPOINT_MIN) || (humidifierSetpoint > HUMIDIFIER_SETPOINT_MAX) ) {
    humidifierSetpoint = HUMIDIFIER_SETPOINT_DEFAULT; //If the value read from EEPROM is out of range, set to default value
    EEPROM.writeInt(addressHumidifierSetpoint,humidifierSetpoint);
    humidifierSetpoint = EEPROM.readInt(addressHumidifierSetpoint);
  }
  g_dynParam_HumidifierSetpoint = humidifierSetpoint;
  
  humidifierTrigger    = EEPROM.read(addressHumidifierTrigger);
  if(humidifierTrigger > 1) {
    humidifierTrigger = ONLINE_TRIGGER;
    EEPROM.write(addressHumidifierTrigger,humidifierTrigger);
    humidifierTrigger    = EEPROM.read(addressHumidifierTrigger);
  }
  g_dynParam_HumidifierTrigger = humidifierTrigger;

  humidifierSchdOffMode    = EEPROM.read(addressHumidifierSchdOffMode);
  if(humidifierSchdOffMode > 1) {
    humidifierSchdOffMode = SCHD_OFF_MODE_AT_VALUE;
    EEPROM.write(addressHumidifierSchdOffMode,humidifierSchdOffMode);
    humidifierSchdOffMode    = EEPROM.read(addressHumidifierSchdOffMode);
  }
  g_dynParam_HumidifierSchdOffMode = humidifierSchdOffMode;
  
//******************************************************************************

  carbonizerSetpoint = EEPROM.readInt(addressCarbonizerSetpoint);
  if( (carbonizerSetpoint < CARBONIZER_SETPOINT_MIN) || (carbonizerSetpoint > CARBONIZER_SETPOINT_MAX) ) {
    carbonizerSetpoint = CARBONIZER_SETPOINT_DEFAULT; //If the value read from EEPROM is out of range, set to default value
    EEPROM.writeInt(addressCarbonizerSetpoint,carbonizerSetpoint);
    carbonizerSetpoint = EEPROM.readInt(addressCarbonizerSetpoint);
  }
  g_dynParam_CarbonizerSetpoint = carbonizerSetpoint;
  
  carbonizerTrigger    = EEPROM.read(addressCarbonizerTrigger);
  if(carbonizerTrigger > 1) {
    carbonizerTrigger = 0;
    EEPROM.write(addressCarbonizerTrigger,carbonizerTrigger);
    carbonizerTrigger    = EEPROM.read(addressCarbonizerTrigger);
  }
  g_dynParam_CarbonizerTrigger = carbonizerTrigger;

  carbonizerSchdOffMode    = EEPROM.read(addressCarbonizerSchdOffMode);
  if(carbonizerSchdOffMode > 1) {
    carbonizerSchdOffMode = 0;
    EEPROM.write(addressCarbonizerSchdOffMode,carbonizerSchdOffMode);
    carbonizerSchdOffMode    = EEPROM.read(addressCarbonizerSchdOffMode);
  }
  g_dynParam_CarbonizerSchdOffMode = carbonizerSchdOffMode;

//******************************************************************************

  ozonizerSetpoint = EEPROM.readInt(addressOzonizerSetpoint);
  if( (ozonizerSetpoint < OZONIZER_SETPOINT_MIN) || (ozonizerSetpoint > OZONIZER_SETPOINT_MAX) ) {
    ozonizerSetpoint = OZONIZER_SETPOINT_DEFAULT; //If the value read from EEPROM is out of range, set to default value
    EEPROM.writeInt(addressOzonizerSetpoint,ozonizerSetpoint);
    ozonizerSetpoint = EEPROM.readInt(addressOzonizerSetpoint);
  }
  g_dynParam_OzonizerSetpoint = ozonizerSetpoint;
  
  ozonizerTrigger    = EEPROM.read(addressOzonizerTrigger);
  if(ozonizerTrigger > 1) {
    ozonizerTrigger = 0;
    EEPROM.write(addressOzonizerTrigger,ozonizerTrigger);
    ozonizerTrigger    = EEPROM.read(addressOzonizerTrigger);
  }
  g_dynParam_OzonizerTrigger = ozonizerTrigger;

  ozonizerSchdOffMode    = EEPROM.read(addressOzonizerSchdOffMode);
  if(ozonizerSchdOffMode > 1) {
    ozonizerSchdOffMode = 0;
    EEPROM.write(addressOzonizerSchdOffMode,ozonizerSchdOffMode);
    ozonizerSchdOffMode    = EEPROM.read(addressOzonizerSchdOffMode);
  }
  g_dynParam_OzonizerSchdOffMode = ozonizerSchdOffMode;

//******************************************************************************

  clarifierSetpoint = EEPROM.readInt(addressClarifierSetpoint);
  if( (clarifierSetpoint < CLARIFIER_SETPOINT_MIN) || (clarifierSetpoint > CLARIFIER_SETPOINT_MAX) ) {
    clarifierSetpoint = CLARIFIER_SETPOINT_DEFAULT; //If the value read from EEPROM is out of range, set to default value
    EEPROM.writeInt(addressClarifierSetpoint,clarifierSetpoint);
    clarifierSetpoint = EEPROM.readInt(addressClarifierSetpoint);
  }
  g_dynParam_ClarifierSetpoint = clarifierSetpoint;
  
  clarifierTrigger    = EEPROM.read(addressClarifierTrigger);
  if(clarifierTrigger > 1) {
    clarifierTrigger = 0;
    EEPROM.write(addressClarifierTrigger,clarifierTrigger);
    clarifierTrigger    = EEPROM.read(addressClarifierTrigger);
  }
  g_dynParam_ClarifierTrigger = clarifierTrigger;

  clarifierSchdOffMode    = EEPROM.read(addressClarifierSchdOffMode);
  if(clarifierSchdOffMode > 1) {
    clarifierSchdOffMode = 0;
    EEPROM.write(addressClarifierSchdOffMode,clarifierSchdOffMode);
    clarifierSchdOffMode    = EEPROM.read(addressClarifierSchdOffMode);
  }
  g_dynParam_ClarifierSchdOffMode = clarifierSchdOffMode;

//******************************************************************************

}


void saveSettings() {
      if(g_dynParam_ConditionerUnits != conditionerUnits)
      {
        EEPROM.write(addressConditionerUnits,g_dynParam_ConditionerUnits);
        conditionerUnits = EEPROM.read(addressConditionerUnits); 
        g_dynParam_ConditionerUnits = conditionerUnits;
      }
      if(g_dynParam_ConditionerUnits != conditionerSetpoint)
      {
        EEPROM.writeFloat(addressConditionerSetpoint,g_dynParam_ConditionerSetpoint);
        conditionerSetpoint = EEPROM.readFloat(addressConditionerSetpoint); 
        g_dynParam_ConditionerSetpoint = conditionerSetpoint;
      }
      if(g_dynParam_ConditionerTrigger != conditionerTrigger)
      {
        EEPROM.write(addressConditionerTrigger,g_dynParam_ConditionerTrigger);
        conditionerTrigger = EEPROM.read(addressConditionerTrigger); 
        g_dynParam_ConditionerTrigger = conditionerTrigger;
      }
      if(g_dynParam_ConditionerSchdOffMode != conditionerSchdOffMode)
      {
        EEPROM.write(addressConditionerSchdOffMode,g_dynParam_ConditionerSchdOffMode);
        conditionerSchdOffMode = EEPROM.read(addressConditionerSchdOffMode); 
        g_dynParam_ConditionerSchdOffMode = conditionerSchdOffMode;
      }
      if( (g_dynParam_ConditionerSchdOnTime[0] != conditionerSchdOnTime[0]) ||
          (g_dynParam_ConditionerSchdOnTime[1] != conditionerSchdOnTime[1]) )
      {
//        EEPROM.writeBlock(addressConditionerSchdOnTime, g_dynParam_ConditionerSchdOnTime, 2);
//        conditionerSchdOnTime = EEPROM.readBlock(addressConditionerSchdOnTime, conditionerSchdOnTime, 2); 
//        g_dynParam_ConditionerSchdOnTime[0] = conditionerSchdOnTime[0];
//        g_dynParam_ConditionerSchdOnTime[1] = conditionerSchdOnTime[1];
      }
      if( (g_dynParam_ConditionerSchdOffTime[0] != conditionerSchdOffTime[0]) ||
          (g_dynParam_ConditionerSchdOffTime[1] != conditionerSchdOffTime[1]) )
      {
//        EEPROM.writeBlock(addressConditionerSchdOffTime, g_dynParam_ConditionerSchdOffTime, 2);
//        conditionerSchdOffTime = EEPROM.readBlock(addressConditionerSchdOffTime, conditionerSchdOffTime, 2); 
//        g_dynParam_ConditionerSchdOffTime[0] = conditionerSchdOffTime[0];
//        g_dynParam_ConditionerSchdOffTime[1] = conditionerSchdOffTime[1];
      }      
      
//********************************************************************************      
      if(g_dynParam_HumidifierSetpoint != humidifierSetpoint)
      {
        EEPROM.writeInt(addressHumidifierSetpoint,g_dynParam_HumidifierSetpoint);
        humidifierSetpoint = EEPROM.readInt(addressHumidifierSetpoint); 
        g_dynParam_HumidifierSetpoint = humidifierSetpoint;
      } 
      if(g_dynParam_HumidifierTrigger != humidifierTrigger)
      {
        EEPROM.write(addressHumidifierTrigger,g_dynParam_HumidifierTrigger);
        humidifierTrigger = EEPROM.read(addressHumidifierTrigger); 
        g_dynParam_HumidifierTrigger = humidifierTrigger;
      }
      if(g_dynParam_HumidifierSchdOffMode != humidifierSchdOffMode)
      {
        EEPROM.write(addressHumidifierSchdOffMode,g_dynParam_HumidifierSchdOffMode);
        humidifierSchdOffMode = EEPROM.read(addressHumidifierSchdOffMode); 
        g_dynParam_HumidifierSchdOffMode = humidifierSchdOffMode;
      }

//********************************************************************************      
      if(g_dynParam_CarbonizerSetpoint != carbonizerSetpoint)
      {
        EEPROM.writeInt(addressCarbonizerSetpoint,g_dynParam_CarbonizerSetpoint);
        carbonizerSetpoint = EEPROM.readInt(addressCarbonizerSetpoint); 
        g_dynParam_CarbonizerSetpoint = carbonizerSetpoint;
      } 
      if(g_dynParam_CarbonizerTrigger != carbonizerTrigger)
      {
        EEPROM.write(addressCarbonizerTrigger,g_dynParam_CarbonizerTrigger);
        carbonizerTrigger = EEPROM.read(addressCarbonizerTrigger); 
        g_dynParam_CarbonizerTrigger = carbonizerTrigger;
      }
      if(g_dynParam_CarbonizerSchdOffMode != carbonizerSchdOffMode)
      {
        EEPROM.write(addressCarbonizerSchdOffMode,g_dynParam_CarbonizerSchdOffMode);
        carbonizerSchdOffMode = EEPROM.read(addressCarbonizerSchdOffMode); 
        g_dynParam_CarbonizerSchdOffMode = carbonizerSchdOffMode;
      }

//********************************************************************************      
      if(g_dynParam_OzonizerSetpoint != ozonizerSetpoint)
      {
        EEPROM.writeInt(addressOzonizerSetpoint,g_dynParam_OzonizerSetpoint);
        ozonizerSetpoint = EEPROM.readInt(addressOzonizerSetpoint); 
        g_dynParam_OzonizerSetpoint = ozonizerSetpoint;
      } 
      if(g_dynParam_OzonizerTrigger != ozonizerTrigger)
      {
        EEPROM.write(addressOzonizerTrigger,g_dynParam_OzonizerTrigger);
        ozonizerTrigger = EEPROM.read(addressOzonizerTrigger); 
        g_dynParam_OzonizerTrigger = ozonizerTrigger;
      }
      if(g_dynParam_OzonizerSchdOffMode != ozonizerSchdOffMode)
      {
        EEPROM.write(addressOzonizerSchdOffMode,g_dynParam_OzonizerSchdOffMode);
        ozonizerSchdOffMode = EEPROM.read(addressOzonizerSchdOffMode); 
        g_dynParam_OzonizerSchdOffMode = ozonizerSchdOffMode;
      }
//********************************************************************************      
      if(g_dynParam_ClarifierSetpoint != clarifierSetpoint)
      {
        EEPROM.writeInt(addressClarifierSetpoint,g_dynParam_ClarifierSetpoint);
        clarifierSetpoint = EEPROM.readInt(addressClarifierSetpoint); 
        g_dynParam_ClarifierSetpoint = clarifierSetpoint;
      } 
      if(g_dynParam_ClarifierTrigger != clarifierTrigger)
      {
        EEPROM.write(addressClarifierTrigger,g_dynParam_ClarifierTrigger);
        clarifierTrigger = EEPROM.read(addressClarifierTrigger); 
        g_dynParam_ClarifierTrigger = clarifierTrigger;
      }
      if(g_dynParam_ClarifierSchdOffMode != clarifierSchdOffMode)
      {
        EEPROM.write(addressClarifierSchdOffMode,g_dynParam_ClarifierSchdOffMode);
        clarifierSchdOffMode = EEPROM.read(addressClarifierSchdOffMode); 
        g_dynParam_ClarifierSchdOffMode = clarifierSchdOffMode;
      }
}
