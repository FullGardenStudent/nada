/*
Climatizer
 - Conditioner - AC will need one input sensor DHT11 and two relay outputs for heater and cooler power - 3 Pins ( 1 One Wire Serial + 2 Digital)
  - Humidifier - Humidifier will take input sensor from same DHT11 and two relay outputs for humidifier and dehumidifier power - 3-1 = 2 pins ( 2 Digital)
  - Carbonizer - Carbonizer will take input sensor MG811 and one relay output for powering the device or solenoid - 2 pins ( 1 Analog + 1 Digital)
  - Ozonizer - Ozonizer will take input sensor MQ131 and one relay output for powering the device or solenoid - 2 pins ( 1 Analog + 1 Digital)
  - Clarifier - Clarifier will take input sensor MQ135 and one relay output for powering the Air Purifier/ Freshner - 2 pins ( 1 Analog + 1 Digital)
Fertigator
  - pHDoser - pH will need one DFRobot pH input sensor and three relay outputs for power, pH- and pH+ - 4 pins ( 1 Analog + 3 Digital)
  - ECDoser - EC will need one DFRobot TDS/ EC sensor and four relay outputs for power, N, P and K  - 5 pins ( 1 Analog + 4 Digital)
Irrigator
  - Water Tank will require one Ultrasonic Water level sensor (trigger and echo) and two output relay for powering the input pump and nutrient pumps - 4 Pins ( 4 Digital)
  - Nutrient Tank will require one Ultrasonic Water level sensor (trigger and echo) and two output relays for powering the mixer, flush and hydroponic pumps - 5 Pins ( 5 Digital)
  - Regulator - Water heat regulator will need one Dallas Temperature sensor and three relay outputs for water heater, hot and cold air fans - 4 Pins ( 1 One Wire + 3 digital)
  - Purifier - Water purifier will need DFRobot Turbidity Sensor and one relay output for purifier - 2 pins ( 1 Analog + 1 Digital)
  - Sterilizer - UV Sterilizer will take input from same turbidity sensor and one output relay for power - 1 Pin ( 1 Digital)
  - Aerator - Aerator will optionally take input from DFRobot Dissolved Oxygen sensor and one relay output for aerator - 2 pins ( 1 Analog + 1 Digital)

Ranges And samples 
Conditioner: Typical - 24 C Day/19 C Night (75 F/65 F), Range: 15 C - 30 C
Regulator - Typical 25C, cool at 26C, heat at 24C, Range: 15 C - 30 C
Humidifier: Typical 60%, Range 0 % - 100%
Carbonizer: Typical 1500 ppm if light is available, ambient (~390 ppm) if not. Range: 0 - 3000 ppm
Luminator: Typical: 17 mol m2/d combination of solar and supplemental light, Range: 0 - 100 mol m2/d
Aerator: Typical: 7 mg/L, Range: 0 mg/L - 100 mg/L
pH - Typical : 5.6, Range: 0 - 14
EC - Typical : 1. 2, Range: 0.0 - 3.6

Change History:
Changed the following in PubSubClient.h
#define MQTT_MAX_PACKET_SIZE 256  //increased from 128 to 256

*/ 

#include <EEPROMex.h>
#include "DHT.h"

//#include <WiFiEspSecureClient.h>
#include <ArduinoJson.h>
#include "Climatizer.h"
#include "Fertigator.h"
#include "Irrigator.h"
#include "HumanInterface.h"
#include "WorldClock.h"
//#include "Device.h"
#include "Actuator.h"
#include "Wire.h"
#include <SoftwareSerial.h>
#include "WiFiEsp.h"
//#include <PubSubClient.h>
//#include <ClickEncoder.h>
#include <TimerOne.h>
#include <TimeLib.h>
#include <TimeAlarms.h>
#include <LiquidCrystal_I2C.h>
#include <LCDMenuLib2.h>
#include <TaskScheduler.h>
#define HAVE_HWSERIAL1
// Emulate Serial1 on pins 6/7 if not present
#ifndef HAVE_HWSERIAL1
//#include "SoftwareSerial.h"
//SoftwareSerial Serial1(6, 7); // RX, TX
#endif

#define APPLIANCE_TELEMETRY "appliance/data/values"


#define ONLINE_TRIGGER     0
#define SCHD_TRIGGER       1
#define SCHD_OFF_MODE_AT_TIME   0
#define SCHD_OFF_MODE_AT_VALUE  1

#define PUMP_OPIN         6
#define VENTILATOR_OPIN   8   

#define LED_BATTEN_ROW1  1
#define LED_BATTEN_ROW2  2
#define LED_BATTEN_ROW3  3
#define LED_BATTEN_ROW4  4
#define LED_BATTEN_ROW5  5

// *********************************************************************
// Global variables
// *********************************************************************
//Device conditioner;
float conditionerReading = 0;
byte conditionerUnits = 0;
float conditionerSetpoint;
byte conditionerTrigger = ONLINE_TRIGGER;
byte conditionerSchdOffMode = SCHD_OFF_MODE_AT_VALUE;
byte conditionerSchdOnTime[2];
byte conditionerSchdOffTime[2];
byte conditionerState = 0;

int  humidifierReading = 0;
int  humidifierSetpoint;
byte humidifierTrigger = ONLINE_TRIGGER;
byte humidifierSchdOffMode = SCHD_OFF_MODE_AT_VALUE;
byte humidifierSchdOnTime[2];
byte humidifierSchdOffTime[2];
byte humidifierState = 0;

//Device carbonizer;
int  carbonizerReading;
int  carbonizerSetpoint;
byte carbonizerTrigger = ONLINE_TRIGGER;
byte carbonizerSchdOffMode = SCHD_OFF_MODE_AT_VALUE;
byte carbonizerSchdOnTime[2];
byte carbonizerSchdOffTime[2];
byte carbonizerState = 0;

//Device ozonizer;
int  ozonizerReading;
int  ozonizerSetpoint;
byte ozonizerTrigger = ONLINE_TRIGGER;
byte ozonizerSchdOffMode = SCHD_OFF_MODE_AT_VALUE;
byte ozonizerSchdOnTime[2];
byte ozonizerSchdOffTime[2];
byte ozonizerState = 0;

//Device clarifier;
int  clarifierReading;
int  clarifierSetpoint;
byte clarifierTrigger = ONLINE_TRIGGER;
byte clarifierSchdOffMode = SCHD_OFF_MODE_AT_VALUE;
byte clarifierSchdOnTime[2];
byte clarifierSchdOffTime[2];
byte clarifierState = 0;

int ventilatorReading = 0;
byte ventilatorState=0;
byte aeratorState;
byte ecDoserState;
byte phDoserState;
byte purifierState;
byte regulatorState;
byte sterilizerState;
byte mixerState;

float balancerReading = 0;
float supplementerReading = 0;
int recyclerReading;
int circulatorReading;

byte lightingCycle;
int parReading,spectrumReading,lightIntensityReading;


float regulatorReading = 0;
float purifierReading = 0;
float aeratorReading = 0;
float sterilizerReading = 0;
float waterFlowReading = 0;
float nutrientFlowReading = 0;
float waterFlowVolume = 0;
float nutrientFlowVolume = 0;
float reservoirLevelReading = 0;
float chamberLevelReading = 0;


float regulatorSetpoint = 25.0;
float purifierSetpoint = 3000;
float aeratorSetpoint = 1;
float sterilizerSetpoint = 1;

float luminatorValue = 0;
String formattedDate;

//float temperature;
int tankStatus=0;

// *********************************************************************
// LCDML display settings
// *********************************************************************
  // settings for LCD
  #define _LCDML_DISP_cols  40
  #define _LCDML_DISP_rows  4

// *********************************************************************
// Prototypes
// *********************************************************************
  void lcdml_menu_display();
  void lcdml_menu_clear();
  void lcdml_menu_control();
// *********************************************************************
// Objects
// *********************************************************************
  LCDMenuLib2_menu LCDML_0 (255, 0, 0, NULL, NULL); // root menu element (do not change)
  LCDMenuLib2 LCDML(LCDML_0, _LCDML_DISP_rows, _LCDML_DISP_cols, lcdml_menu_display, lcdml_menu_clear, lcdml_menu_control);

//Variables used in Menus
float g_dynParam_ConditionerSetpoint;
byte g_dynParam_ConditionerUnits = 0;
byte g_dynParam_ConditionerTrigger = 0;
byte g_dynParam_ConditionerSchdOffMode = 0;
byte g_dynParam_ConditionerSchdOnTime[2];
byte g_dynParam_ConditionerSchdOffTime[2];

int  g_dynParam_HumidifierSetpoint;
byte g_dynParam_HumidifierTrigger = 0;
byte g_dynParam_HumidifierSchdOffMode = 0;
byte g_dynParam_HumidifierSchdOnTime[2];
byte g_dynParam_HumidifierSchdOffTime[2];

int  g_dynParam_CarbonizerSetpoint;
byte g_dynParam_CarbonizerTrigger = 0;
byte g_dynParam_CarbonizerSchdOffMode = 0;
byte g_dynParam_CarbonizerSchdOnTime[2];
byte g_dynParam_CarbonizerSchdOffTime[2];

int  g_dynParam_OzonizerSetpoint;
byte g_dynParam_OzonizerTrigger = 0;
byte g_dynParam_OzonizerSchdOffMode = 0;
byte g_dynParam_OzonizerSchdOnTime[2];
byte g_dynParam_OzonizerSchdOffTime[2];

int  g_dynParam_ClarifierSetpoint;
byte g_dynParam_ClarifierTrigger = 0;
byte g_dynParam_ClarifierSchdOffMode = 0;
byte g_dynParam_ClarifierSchdOnTime[2];
byte g_dynParam_ClarifierSchdOffTime[2];

float g_dynParam_RegulatorSetpoint;
byte g_dynParam_RegulatorUnits = 0;
byte g_dynParam_RegulatorTrigger = 0;
byte g_dynParam_RegulatorSchdOffMode = 0;
byte g_dynParam_RegulatorSchdOnTime[2];
byte g_dynParam_RegulatorSchdOffTime[2];

int  g_dynParam_PurifierSetpoint;
byte g_dynParam_PurifierUnits = 0;
byte g_dynParam_PurifierTrigger = 0;
byte g_dynParam_PurifierSchdOffMode = 0;
byte g_dynParam_PurifierSchdOnTime[2];
byte g_dynParam_PurifierSchdOffTime[2];


unsigned long g_timer_1000ms = 0;
uint8_t dyn_hour = 0;
uint8_t dyn_min  = 0;
uint8_t dyn_sec  = 0;
uint8_t dyn_day = 0;
uint8_t dyn_month  = 0;
uint8_t dyn_year  = 0;
//********************************************************************************
boolean settingChanged = 0;


DHT dht(DHT11_IPIN, DHTTYPE);


//--------------------------------------------------------------
//Flow sensor variables
volatile int  inflow_frequency;  // Measures flow meter pulses
volatile int  outflow_frequency;  // Measures flow meter pulses
//volatile byte pulseCount;  
float inflowRate;
unsigned int inflowMilliLitres;
unsigned long intotalMilliLitres;

float outflowRate;
unsigned int outflowMilliLitres;
unsigned long outtotalMilliLitres;
//--------------------------------------------------------------




SoftwareSerial serialGateway(2, 3); // RX, TX
  
// Update these with values suitable for your network.
char ssid[] = "prayasa";            // your network SSID (name)
char password[] = "gearhead";        // your network password
//char ssid[] = "Bottu's";            // your network SSID (name)
//char password[] = "9eaf66c4";        // your network password

int status = WL_IDLE_STATUS;     // the Wifi radio's status

char server[] = "cenauraiotpoc.azurewebsites.net";

// Initialize the Ethernet client object
WiFiEspClient client;

//WiFiEspServer server(80);

// use a ring buffer to increase speed and reduce memory allocation
RingBuffer buf(8);

//WiFiEspClient espClient;
WiFiEspSecureClient espClient;
//PubSubClient psClient(espClient);
long lastMsg = 0;
char msg[50];

int built_in_led = 13;
int ledStatus = LOW;

float warmerReading;
float warmerSetpoint;
int warmerState = 0;
float ecDoserValue;
float ecDoserSetpoint;
//int ecDoserState = 0;
float phDoserValue;
float phDoserSetpoint;
//int phDoserState = 0;

// Variables
int x, y;
int page = 0;
int tankProgState = 0;
int manualRefilState = 0;

static int count;
static uint32_t last;

int pHPin = A9;                    //pin for pH probe
int pHPlusPin = 45;                //pin for Base pump (relay)
int pHMinPin = 43;                 //pin for Acide pump (relay)
int pumpPin = 47;           //pin for main pump

uint8_t pHMinRangeParam = 0;
uint8_t pHMaxRangeParam = 14;
float pHSetpoint;                    //holds value for pHSetpoint

float FanTemp;
float FanHumid;
int flood;
// Variables for pulsing the pump
long previousMillis = 0;           //             |
long pinHighTime = 100;            //             |
long pinLowTime = 7500;            //             |
long pinTime = 100;                //             |

int EeprompHSetpoint = 10;      //location of pHSetpoint in Eeprom
int EepromSetpHHysteris = 20;   //location of SetpHHysteris in Eeprom


bool flagWifiConnected = 0;


//char* getWaterFlowRate()
float getWaterFlowRate()
{
//   currentTime = millis();
   float ilph;
   // Pulse frequency (Hz) = 7.5Q, Q is flow rate in L/min. (Results in +/- 3% range)
   ilph = (inflow_frequency / 7.5); // (Pulse frequency x 60 min) / 7.5Q = flow rate in L/min 
   inflow_frequency = 0;                   // Reset Counter
//   ilph = roundDecimalPoint(ilph,2);
   inflowRate = ilph;
//  return "";
  return ilph;
}

//char* getWaterFlowVolume()
float getWaterFlowVolume()
{
  // Divide the flow rate in litres/minute by 60 to determine how many litres have
  // passed through the sensor in this 1 second interval, then multiply by 1000 to
  // convert to millilitres.
  float inflowMilliLitres = (inflowRate / 60) * 1000;
  // Add the millilitres passed in this second to the cumulative total
  intotalMilliLitres += inflowMilliLitres;
  //unsigned int frac;
  float totalinflowLitres = intotalMilliLitres/1000;
  // Reset the pulse counter so we can start incrementing again
  //pulseCount = 0;
//  totalinflowLitres = roundDecimalPoint(totalinflowLitres,2);
// return "";
 return totalinflowLitres;
}


//char* getNutrientFlowRate()
float getNutrientFlowRate()
{
//   currentTime = millis();
   float ilph;
   // Pulse frequency (Hz) = 7.5Q, Q is flow rate in L/min. (Results in +/- 3% range)
   ilph = (outflow_frequency / 7.5); // (Pulse frequency x 60 min) / 7.5Q = flow rate in L/min 
   outflow_frequency = 0;                   // Reset Counter
//   ilph = roundDecimalPoint(ilph,2);
   outflowRate = ilph;
//  return "";
  return ilph;
}

//char* getWaterFlowVolume()
float getNutrientFlowVolume()
{
  // Divide the flow rate in litres/minute by 60 to determine how many litres have
  // passed through the sensor in this 1 second interval, then multiply by 1000 to
  // convert to millilitres.
  float outflowMilliLitres = (outflowRate / 60) * 1000;
  // Add the millilitres passed in this second to the cumulative total
  outtotalMilliLitres += outflowMilliLitres;
  //unsigned int frac;
  float totaloutflowLitres = outtotalMilliLitres/1000;
  // Reset the pulse counter so we can start incrementing again
  //pulseCount = 0;
//  totalinflowLitres = roundDecimalPoint(totalinflowLitres,2);
// return "";
 return totaloutflowLitres;
}


void inflow ()                  // Interrupt function
{ 
   inflow_frequency++;
} 

void outflow ()                  // Interrupt function
{ 
   outflow_frequency++;
}

// *********************************************************************
// Task Scheduler
// *********************************************************************

  // objects
  Scheduler r, hpr;

  // Callback methods prototypes
  void Task_Serial_Blink_Example(); 
  void Task_LCDMenuLib();

  // Tasks
  Task t1(1000,  TASK_FOREVER, &Task_ReadTime, &r);
  Task t2(30000,  TASK_FOREVER, &Task_ReadDHTsensor, &r);
  Task t3(60000,  TASK_FOREVER, &Task_ReadCarbonizer, &r);
  Task t4(60000,  TASK_FOREVER, &Task_ReadOzonizer, &r);
  Task t5(60000,  TASK_FOREVER, &Task_ReadClarifier, &r);
  Task t6(60000,  TASK_FOREVER, &Task_ReadReservoirLevel, &r);
//  Task t7(60000,  TASK_FOREVER, &Task_ReadChamberLevel, &r);
  Task t8(60000,  TASK_FOREVER, &Task_ReadPurifier, &r);
//  Task t9(30000,  TASK_FOREVER, &Task_ReadRegulator, &r);
  Task t10(10000,  TASK_FOREVER, &Task_ReadWaterFlow, &r);
  Task t11(10000,  TASK_FOREVER, &Task_ReadNutrientFlow, &r);
  Task t12(3000,  TASK_FOREVER, &Task_CheckChangeInSettings, &r);
  Task t13(20000,  TASK_FOREVER, &Task_SendTelemetry, &r);
  
  Task t14(60000,  TASK_FOREVER, &Task_ReadRegulator, &r);
  Task t15(60000,  TASK_FOREVER, &Task_ReadChamberLevel, &r);
  Task t16(60000,  TASK_FOREVER, &Task_ReadNutrient, &r);
  Task t17(60000,  TASK_FOREVER, &Task_ReadpH, &r);

//  Task t14(3000,  TASK_FOREVER, &Task_Screensaver, &r);
  Task t18(300000,  TASK_FOREVER, &Task_switchVentilator, &r);


//  Task t14(100,  TASK_FOREVER, &Task_LCDML, &r);

  
  
//***********************************************************************  
DateTime nowTime;
void Task_ReadTime() {
  nowTime = WorldClock.readTime();
  WorldClock.printTime(nowTime);

    dyn_hour = nowTime.hour();
    dyn_min = nowTime.minute();
    dyn_sec = nowTime.second();
    dyn_day = nowTime.day();
    dyn_month = nowTime.month();
    dyn_year = nowTime.year();
}
//***********************************************************************
void Task_ReadDHTsensor() {
    // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
   humidifierReading = dht.readHumidity();
  // Read temperature as Celsius (the default)
  conditionerReading = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(humidifierReading) || isnan(conditionerReading) ) {
    Serial.println(F("Failed to read from DHT sensor!"));
  }
  Serial.print("Humidity: ");
  Serial.print(humidifierReading);
  Serial.println("%");
  
  Serial.print("Temperature: ");
  Serial.print(conditionerReading);
  Serial.println("°C ");
  Serial.println("");  
}
//***********************************************************************
void Task_ReadCarbonizer() {
  carbonizerReading = Climatizer.readCarbonizer();
  Climatizer.carbonizer.reading = Climatizer.readCarbonizer();
  Climatizer.triggerCarbonizer();
  Serial.print("CO2: ");
  Serial.print(carbonizerReading);
  Serial.println("ppm");

}
//***********************************************************************
void Task_ReadOzonizer() {
  ozonizerReading = Climatizer.readOzonizer();
  Serial.print("Ozone: ");
  Serial.print(ozonizerReading);
  Serial.println("ppm");
}
//***********************************************************************
void Task_ReadClarifier() {
  clarifierReading = Climatizer.readClarifier();
  Serial.print("Air Quality: ");
  Serial.print(clarifierReading);
  Serial.println("ppm");
}

//***********************************************************************
void Task_ReadReservoirLevel() {
  reservoirLevelReading = Irrigator.readReservoirLevel();
  Serial.print("Reservoir level : ");
  Serial.println(reservoirLevelReading);
}

//***********************************************************************
//void Task_ReadChamberLevel() {
//  chamberLevelReading = Fertigator.readChamberLevel();
//  Serial.print("Chamber level : ");
//  Serial.println(chamberLevelReading); 
//}
//***********************************************************************
void Task_ReadPurifier() {
  purifierReading = Irrigator.readPurifier();
  Serial.print("Purifier : ");
  Serial.println(purifierReading); 
}

//***********************************************************************
//void Task_ReadRegulator() {
//  regulatorReading = Fertigator.readRegulator(); //Read the sensor
//  Serial.print("Water Temperature : ");
//  Serial.print(regulatorReading);
//  Serial.println("'C");
//}

//***********************************************************************
void Task_ReadWaterFlow() {
  waterFlowReading = getWaterFlowRate();
  Serial.print("Water Flow Rate : ");
  Serial.print(waterFlowReading);
  Serial.println("l/h");

  waterFlowVolume = getWaterFlowVolume();
  Serial.print("Water Flow Volume : ");
  Serial.print(waterFlowVolume);
  Serial.println("litres");
} 
//***********************************************************************
void Task_ReadNutrientFlow() {
  nutrientFlowReading = getNutrientFlowRate();
  Serial.print("Nutrient Flow Rate : ");
  Serial.print(nutrientFlowReading);
  Serial.println("l/h");

  nutrientFlowVolume = getNutrientFlowVolume();
  Serial.print("Nutrient Flow Volume : ");
  Serial.print(nutrientFlowVolume);
  Serial.println("litres");
}

//***********************************************************************
void Task_CheckChangeInSettings() {
    if(settingChanged == 1)
    {

      saveSettings();
       settingChanged = 0;
    }
}
//***********************************************************************
void Task_SendTelemetry() {
  buildApplianceTelemetry(); 
  sendHTTPTelemetry();
}

void Task_ReadRegulator() {
  warmerReading = Fertigator.readRegulator(); //Read the sensor
  warmerState = Fertigator.readRegulatorState();
  Serial.print("Temperature: ");
  Serial.print(warmerReading);
  Serial.println("'C");
}

void Task_ReadChamberLevel() {
  Fertigator.readChamberLevel();
  Serial.println("Checking Chamber Level");
  //status = Fertigator.readTankLevel(1);
  if (status==HIGH){
    Serial.println("Chamber full");
  }
  if (status==LOW){
    Serial.println("Chamber empty");
  }
}

void Task_ReadNutrient() {
  Fertigator.regulateNutrient(25.0); // default setpoint so temp can be maintained between 24-26 C
 
  delay(500);

  float resistanceEC = Fertigator.readResistanceEC(); //EC Value in resistance
  Serial.print(F("EC Value in resistance = "));
  Serial.print(resistanceEC);  

  Serial.print(F(" // EC Value = "));
  float EC = Fertigator.ECConversion(resistanceEC); //EC Value in µS/cm
  Serial.print(EC);
  Serial.println(F(" uS/cm"));
  ecDoserValue = EC;
}

void Task_ReadpH()
{   
  int mvpH = Fertigator.readpH(); //Value in mV of pH
  Serial.print(F("pH Value in mV = "));
  Serial.print(mvpH);

  Serial.print(F(" // pH = "));
  float pH = Fertigator.pHConversion(mvpH); //Calculate pH value
  Serial.println(pH);
  phDoserValue = pH;
}


//void Task_LCDML() {
//  LCDML.loop();
//}
//***********************************************************************

void setup(){
  //setTime(now.hour(), now.minute(), now.second(), now.day(), now.month(),now.year());
  pinMode(built_in_led, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(SHIFT_REG_LATCH, OUTPUT);
  pinMode(SHIFT_REG_DATA, OUTPUT);  
  pinMode(SHIFT_REG_CLK, OUTPUT);  
  
  pinMode(WATER_LEVEL_IPIN, INPUT);
  pinMode(WATER_LEVEL_OPIN, OUTPUT);
  pinMode(CHAMBER_LEVEL_IPIN, INPUT);
  pinMode(CHAMBER_LEVEL_OPIN, OUTPUT);
  
  pinMode(WATER_FLOW_IPIN, INPUT);
  pinMode(NUTRIENT_FLOW_IPIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(NUTRIENT_FLOW_IPIN), outflow, RISING);
  attachInterrupt(digitalPinToInterrupt(WATER_FLOW_IPIN), inflow, RISING); // Setup Interrupt // DO NOT REMOVE
                                     // see http://arduino.cc/en/Reference/attachInterrupt
  sei();    
  Serial.begin(115200);
  Serial3.begin(115200);
  
  HumanInterface.welcome();
  HumanInterface.playTone();
  
  Serial.println("Hello");


  setup_EEPROM();
   // initialize ESP module
  WiFi.init(&Serial3);
  setup_wifi();
   
  dht.begin();

  Actuator.init();
  Climatizer.init(); 
  Fertigator.init(); 

  WorldClock.initRTC();

//WorldClock.setupTime();
//  WorldClock.synchronizeHostTime();
  // Doser related setup
  MCUSR = 0;
  //wdt_disable();
  //wdt_reset();  //Watch dog threshold reset
  noScreenSetup();
  EepromRead();

  Actuator.init();

  delay(500);

   // Enable  Read time Task
    t1.enable(); 
    t2.enable();  
    t3.enable();  
    t4.enable();  
    t5.enable();  
    t6.enable();  
//    t7.enable();  
    t8.enable();  
//    t9.enable();  
    t10.enable();  
    t11.enable();  
    t12.enable();   //Task_CheckChangeInSettings
    t13.enable(); //Task_sendTelemetry
    t14.enable();
    t15.enable();
    t16.enable();
    t17.enable();
  t18.enable();

  delay(500);
  LCD_setup();
//    t14.enable();

  nowTime = WorldClock.readTime();
  setTime(nowTime.hour(), nowTime.minute(), nowTime.second(),nowTime.month(), nowTime.day(), nowTime.year());

  // create the alarms, to trigger at specific times
  Alarm.alarmRepeat(8,29,0, prepareForNutrientAlarm);  // 8:29am every day
  Alarm.alarmRepeat(8,30,0, feedNutrientAlarm);  // 8:30am every day
  
  Alarm.alarmRepeat(22,29,0, prepareForNutrientAlarm);  // 10:29pm every day
  Alarm.alarmRepeat(22,30,0, feedNutrientAlarm);  // 10:30pm every day
  Alarm.alarmRepeat(dowSaturday,12,0,0,flushChamberWeeklyAlarm);  // 12:00:00 noon every Saturday

  Alarm.alarmRepeat(14400, switchPumpONAlarm);  // repeats every 4hrs  //4hrs = 14400S

  Alarm.alarmRepeat(9,0,0, switchONLightsAlarm);  // 9am every day
  Alarm.alarmRepeat(21,0,0, switchOFFLightsAlarm);  // 9pm every day

  Alarm.alarmRepeat(14400, switchPumpONAlarm);  // repeats every 4hrs  //4hrs = 14400S

  Alarm.alarmRepeat(9,0,0, switchONLightsAlarm);  // 9am every day
  Alarm.alarmRepeat(21,0,0, switchOFFLightsAlarm);  // 9pm every day
  Alarm.alarmRepeat(5,50,0, MyTestAlarm);  // 5:50am every day

}


void MyTestAlarm()
{
     Serial.println("My Test Alarm");
}

void Task_switchVentilator()
{
  if(ventilatorState == 0)
  {
    ventilatorState = 1;
     switchON(actuators.data,VENTILATOR_OPIN);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
  }
  else
  {
     switchOFF(actuators.data,VENTILATOR_OPIN);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
     ventilatorState = 0;
  }
}

void feedNutrientAlarm()
{
  Fertigator.feedNutrient(); 
}

void prepareForNutrientAlarm()
{
  Fertigator.prepareForNutrient();
}
void flushChamberWeeklyAlarm()
{
  Fertigator.flushChamber(); 
//    Fertigator.flushChamber();
}

void switchPumpONAlarm()
{
//      digitalWrite(pumpPin, HIGH);
   switchON(actuators.data,PUMP_OPIN);  //Clear bit to switch ON the relay
   Actuator.updateShiftRegister();
      flood = 1;
      Serial.println("Flooding: on");
  Alarm.timerOnce(1800, switchPumpOFFAlarm);            // called once after 30x60=1800 seconds

}
void switchPumpOFFAlarm()
{
//      digitalWrite(pumpPin, HIGH);
   switchOFF(actuators.data,PUMP_OPIN);  //Clear bit to switch ON the relay
   Actuator.updateShiftRegister();
      flood = 0;
      Serial.println("Flooding: off");
}

void switchONLightsAlarm()
{
     switchON(actuators.data,LED_BATTEN_ROW1);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 1 ON");
      delay(2000); // Wait 2 seconds
      
     switchON(actuators.data,LED_BATTEN_ROW2);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 2 ON");
      delay(2000); // Wait 2 seconds
      
     switchON(actuators.data,LED_BATTEN_ROW3);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 3 ON");
      delay(2000); // Wait 2 seconds

     switchON(actuators.data,LED_BATTEN_ROW4);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 4 ON");
      delay(2000); // Wait 2 seconds

     switchON(actuators.data,LED_BATTEN_ROW5);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 5 ON");
      delay(2000); // Wait 2 seconds
}

void switchOFFLightsAlarm()
{
     switchOFF(actuators.data,LED_BATTEN_ROW1);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 1 OFF");
      delay(2000); // Wait 2 seconds
      
     switchOFF(actuators.data,LED_BATTEN_ROW2);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 2 OFF");
      delay(2000); // Wait 2 seconds
      
     switchOFF(actuators.data,LED_BATTEN_ROW3);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 3 OFF");
      delay(2000); // Wait 2 seconds

     switchOFF(actuators.data,LED_BATTEN_ROW4);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 4 OFF");
      delay(2000); // Wait 2 seconds

     switchOFF(actuators.data,LED_BATTEN_ROW5);  //Clear bit to switch ON the relay
     Actuator.updateShiftRegister();
      Serial.println("LED BATTEN 5 OFF");
      delay(2000); // Wait 2 seconds

}





void runPump()
{
  DateTime now = WorldClock.readTime();
  if ((now.hour() % 2 == 1) && (now.minute() == 0 || now.minute() == 1 || now.minute() == 2 || now.minute() == 3 || now.minute() == 4))
  {
    //Starts at 7am
    if ((now.hour() - 7) >= 0)
    {
      digitalWrite(pumpPin, HIGH);
      flood = 1;
      Serial.println("Flooding: on");
    }
  }
  else
  {
    digitalWrite(pumpPin, LOW);
    flood = 0;
    Serial.println("Flooding: off");
  }

}

double Fahrenheit(double celsius)
{
  return 1.8 * celsius + 32;
}

void noScreenSetup() {
  EEPROM.writeFloat(EeprompHSetpoint, 6.5);  // 6.5 pH for tomato growth
  EEPROM.writeFloat(EepromSetpHHysteris, 1.0);
}

void EepromRead()
{
  //pHSetpoint = EEPROM.readFloat(EeprompHSetpoint);
  //SetpHHysteris = EEPROM.readFloat(EepromSetpHHysteris);
}

void pHLogicSetup()
{

//  pinMode(pHPlusPin, OUTPUT);
//  pinMode(pHMinPin, OUTPUT);
//  pinMode(pumpPin, OUTPUT);
}

//****************************************************************
void loop(){

  r.execute();
  LCDML.loop();
  applianceLoop();
  
//  delay(100);
  Alarm.delay(0);   //Do not delete, needed for Alarm

  if(flagWifiConnected == 0)
  {
    flagWifiConnected = 1;
    setup_wifi();
  }

}


void applianceLoop() {
  Climatizer.conditioner.mode = conditionerTrigger;
  Climatizer.conditioner.schdONtime[0] = conditionerSchdOnTime[0];
  Climatizer.conditioner.schdONtime[1] = conditionerSchdOnTime[1];
  Climatizer.humidifier.mode = humidifierTrigger;
  Climatizer.humidifier.schdONtime[0] = humidifierSchdOnTime[0];
  Climatizer.humidifier.schdONtime[1] = humidifierSchdOnTime[1];
  Climatizer.carbonizer.mode = carbonizerTrigger;
  Climatizer.carbonizer.schdONtime[0] = carbonizerSchdOnTime[0];
  Climatizer.carbonizer.schdONtime[1] = carbonizerSchdOnTime[1];
  Climatizer.ozonizer.mode = ozonizerTrigger;
  Climatizer.ozonizer.schdONtime[0] = ozonizerSchdOnTime[0];
  Climatizer.ozonizer.schdONtime[1] = ozonizerSchdOnTime[1];
  Climatizer.clarifier.mode = clarifierTrigger;
  Climatizer.clarifier.schdONtime[0] = clarifierSchdOnTime[0];
  Climatizer.clarifier.schdONtime[1] = clarifierSchdOnTime[1];

  Climatizer.Loop();
//  fertigatorLoop();
//  irrigatorLoop();
//  luminatorLoop();
}


//void relayBoardTest()
//{
//    switchON(actuators.data,LED_BATTEN_ROW1);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 1 ON");
//      
//     switchON(actuators.data,LED_BATTEN_ROW2);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 2 ON");
// 
//     switchON(actuators.data,LED_BATTEN_ROW3);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 3 ON");
// 
//      switchON(actuators.data,LED_BATTEN_ROW4);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 4 ON");
// 
//     switchON(actuators.data,LED_BATTEN_ROW5);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 5 ON");
//     switchON(actuators.data,LED_BATTEN_ROW6);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//     switchON(actuators.data,LED_BATTEN_ROW7);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//     switchON(actuators.data,LED_BATTEN_ROW8);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      delay(5000); // Wait 2 seconds
//
//    switchOFF(actuators.data,LED_BATTEN_ROW1);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 1 OFF");
//      
//     switchOFF(actuators.data,LED_BATTEN_ROW2);  //Clear bit to switch OFF the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 2 OFF");
// 
//     switchOFF(actuators.data,LED_BATTEN_ROW3);  //Clear bit to switch OFF the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 3 OFF");
// 
//      switchOFF(actuators.data,LED_BATTEN_ROW4);  //Clear bit to switch OFF the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 4 OFF");
// 
//     switchOFF(actuators.data,LED_BATTEN_ROW5);  //Clear bit to switch OFF the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 5 OFF");
//     switchOFF(actuators.data,LED_BATTEN_ROW6);  //Clear bit to switch OFF the relay
//     Actuator.updateShiftRegister();
//     switchOFF(actuators.data,LED_BATTEN_ROW7);  //Clear bit to switch OFF the relay
//     Actuator.updateShiftRegister();
//     switchOFF(actuators.data,LED_BATTEN_ROW8);  //Clear bit to switch OFF the relay
//     Actuator.updateShiftRegister();
//      delay(5000); // Wait 2 seconds
//
//   switchON(actuators.data,LED_BATTEN_ROW1);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 1 OFF");
//      delay(2000); // Wait 2 seconds
//      
//   switchON(actuators.data,LED_BATTEN_ROW2);  //Clear bit to switch ON the relay
//     switchOFF(actuators.data,LED_BATTEN_ROW1);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 2 OFF");
//      delay(5000); // Wait 2 seconds
//
//     switchON(actuators.data,LED_BATTEN_ROW3);  //Clear bit to switch ON the relay
//    switchOFF(actuators.data,LED_BATTEN_ROW2);  //Clear bit to switch ON the relay
//    Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 1 ON");
//      delay(2000); // Wait 2 seconds
//      
//     switchON(actuators.data,LED_BATTEN_ROW4);  //Clear bit to switch ON the relay
//    switchOFF(actuators.data,LED_BATTEN_ROW3);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 2 ON");
//      delay(5000); // Wait 2 seconds
// 
//     switchON(actuators.data,LED_BATTEN_ROW5);  //Clear bit to switch ON the relay
//    switchOFF(actuators.data,LED_BATTEN_ROW4);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 1 OFF");
//      delay(2000); // Wait 2 seconds
//      
//     switchON(actuators.data,LED_BATTEN_ROW6);  //Clear bit to switch ON the relay
//     switchOFF(actuators.data,LED_BATTEN_ROW5);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 2 OFF");
//      delay(5000); // Wait 2 seconds
//
//     switchON(actuators.data,LED_BATTEN_ROW7);  //Clear bit to switch ON the relay
//    switchOFF(actuators.data,LED_BATTEN_ROW6);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 1 ON");
//      delay(2000); // Wait 2 seconds
//      
//     switchON(actuators.data,LED_BATTEN_ROW8);  //Clear bit to switch ON the relay
//    switchOFF(actuators.data,LED_BATTEN_ROW7);  //Clear bit to switch ON the relay
//     Actuator.updateShiftRegister();
//      Serial.println("LED BATTEN 2 ON");
//      delay(2000); // Wait 2 seconds
//    switchOFF(actuators.data,LED_BATTEN_ROW8);  //Clear bit to switch ON the relay
//}
